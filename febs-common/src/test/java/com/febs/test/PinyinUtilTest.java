package com.febs.test;

import org.junit.Test;

import com.febs.common.utils.PinyinUtil;

public class PinyinUtilTest {
	@Test
	public void testQuanPin() {
		String res = PinyinUtil.getFullPinyin("产品中心");
		System.out.println(res);
	}
	@Test
	public void testAAQuanPin() {
		String res = PinyinUtil.getFullPinyin("产品中心,XL,1.2*1.5");
		System.out.println(res);
	}
}
