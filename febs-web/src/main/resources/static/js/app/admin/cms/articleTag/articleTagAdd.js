var validator;
var $articleTagAddForm = $("#articleTag-add-form");
$(function () {
	validateRule();
    $("#articleTag-add .btn-save").click(function () {
        var name = $(this).attr("name");
        var validator = $articleTagAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/articleTag/add", $articleTagAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.n_success(r.msg);
                        $MB.refreshTable("articleTagTable");
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/articleTag/update", $articleTagAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.n_success(r.msg);
                        $MB.refreshTable("articleTagTable");
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#articleTag-add .btn-close").click(function () {
        closeModal();
    });

});

function closeModal() {
    $("#articleTag-add-button").attr("name", "save");
    validator.resetForm();
    $("#articleTag-add-modal-title").html('新增标签');
    $MB.closeAndRestModal("articleTag-add");

}

function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $articleTagAddForm.validate({
        rules: {
            tagName: {
            	required: true
            }
        },
        messages: {
            title: icon + "请输入标签名称",
        }
    });
}
