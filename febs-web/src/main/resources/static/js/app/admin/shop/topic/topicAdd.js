var validator;
var $topicAddForm = $("#topic-add-form");
$(function () {
    validateRule();
    $topicAddForm.find("input[name='template']").val("productList.html");
    $("#topic-add .btn-save").click(function () {
        var name = $(this).attr("name");
        var validator = $topicAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/topic/add", $topicAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.n_success(r.msg);
                        $MB.refreshTable("topicTable");
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/topic/update", $topicAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.n_success(r.msg);
                        $MB.refreshTable("topicTable");
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#topic-add .btn-close").click(function () {
        closeModal();
    });

});

function closeModal() {
    $("#topic-add-button").attr("name", "save");
    validator.resetForm();
    $topicAddForm.find("input[name='template']").val("productList.html");
    $topicAddForm.find("input[name='recommend']").prop("checked", true);
    $("#topic-add-modal-title").html('新增专题');
    $MB.closeAndRestModal("topic-add");

}
$('#uploadTopic').fileupload({
    autoUpload: true,//自动上传
    url: "/cms/filesUpload/thumbnail",//ַ上传图片对应的地址
    dataType: 'json',
    done: function (e, data) {
        var oimage = data;
        $("#thumbnail").val(oimage.result.msg.src);
      }
})
function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $topicAddForm.validate({
        rules: {
            name: {
                required: true
            },
            template: {
            	required: true
            }
        },
        errorPlacement: function (error, element) {
            if (element.is(":checkbox") || element.is(":radio")) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
        	name: {
                required: icon + "请输入专题名称"
            },
            template:{
            	required:icon +"请输入显示模板"
            }
        }
    });
}