var validator;
var $productCommentAddForm = $("#productComment-add-form");
$(function () {
    var options = {
    		single: true,
            width: '100%'
        };
    validateRule();
    $("#productComment-add .btn-save").click(function () {
        var name = $(this).attr("name");
        validator = $productCommentAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/productComment/add", $productCommentAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.refreshTable("productCommentTable");
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/productComment/update", $productCommentAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.refreshTable("productCommentTable");
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#productComment-add .btn-close").click(function () {
        closeModal();
    });

});
function closeModal() {
    $("#productComment-add-button").attr("name", "save");
    $("#productComment-add-modal-title").html('新增评论');
    validator.resetForm();
    $MB.closeAndRestModal("productComment-add");
}

function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $productCommentAddForm.validate({
        rules: {
            content: {
                required: true
            }
        },
        messages: {
        	content: {
                required: icon + "请输入评论名称",
                minlength: icon + "评论名称长度10到100个字符",
                remote: icon + "该评论名称已经存在"
            }
        }
    });
}