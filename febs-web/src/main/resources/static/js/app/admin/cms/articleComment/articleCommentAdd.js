var validator;
var $articleCommentAddForm = $("#articleComment-add-form");
$(function () {
    var options = {
    		single: true,
            width: '100%'
        };
    validateRule();
    $("#articleComment-add .btn-save").click(function () {
        var name = $(this).attr("name");
        validator = $articleCommentAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/articleComment/add", $articleCommentAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.refreshTable("articleCommentTable");
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/articleComment/update", $articleCommentAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.refreshTable("articleCommentTable");
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#articleComment-add .btn-close").click(function () {
        closeModal();
    });

});
function closeModal() {
    $("#articleComment-add-button").attr("name", "save");
    $("#articleComment-add-modal-title").html('新增评论');
    validator.resetForm();
    $MB.closeAndRestModal("articleComment-add");
}

function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $articleCommentAddForm.validate({
        rules: {
            content: {
                required: true
            }
        },
        messages: {
        	content: {
                required: icon + "请输入评论名称",
                minlength: icon + "评论名称长度10到100个字符",
                remote: icon + "该评论名称已经存在"
            }
        }
    });
}