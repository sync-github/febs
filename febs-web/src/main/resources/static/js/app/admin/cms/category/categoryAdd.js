var validator;
var $categoryAddForm = $("#category-add-form");
$(function () {
    var options = {
    		single: true,
            width: '100%'
        };
    validateRule();
    createCategoryTree();
    $categoryAddForm.find("input[name='template']").val("articleList.html");
    $("#category-add .btn-save").click(function () {
        var name = $(this).attr("name");
        getCategory();
        validator = $categoryAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/category/add", $categoryAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        refresh();
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/category/update", $categoryAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        refresh();
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#category-add .btn-close").click(function () {
        closeModal();
    });

});
function closeModal() {
    $("#category-add-button").attr("name", "save");
    $("#category-add-modal-title").html('新增分类');
    validator.resetForm();
    $MB.closeAndRestModal("category-add");
    $categoryAddForm.find("input[name='template']").val("articleList.html");
    $MB.refreshJsTree("categoryTree", createCategoryTree());
}

function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $categoryAddForm.validate({
        rules: {
            name: {
                required: true
            },
            template:{
            	required:true
            }
        },
        messages: {
            name: {
                required: icon + "请输入分类名称",
                minlength: icon + "分类名称长度3到10个字符",
                remote: icon + "该分类名称已经存在"
            },
            template:{
            	required: icon + "请输入模板名称"
            }
        }
    });
}

function createCategoryTree() {
    $.post(ctx + "admin/category/tree", {}, function (r) {
        if (r.code === 0) {
            var data = r.msg;
            $('#categoryTree').jstree({
                "core": {
                    'data': data.children,
                    'multiple': false
                },
                "state": {
                    "disabled": true
                },
                "checkbox": {
                    "three_state": false
                },
                "plugins": ["wholerow", "checkbox"]
            });
        } else {
            $MB.n_danger(r.msg);
        }
    })

}

function getCategory() {
    var ref = $('#categoryTree').jstree(true);
    var categoryIds = ref.get_checked();
    $("[name='parentId']").val(categoryIds[0]);
}