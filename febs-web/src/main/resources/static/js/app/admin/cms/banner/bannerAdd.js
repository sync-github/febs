var validator;
var $bannerAddForm = $("#banner-add-form");
$(function () {
    validateRule();

    $("#banner-add .btn-save").click(function () {
        var name = $(this).attr("name");
        var validator = $bannerAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/banner/add", $bannerAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.n_success(r.msg);
                        $MB.refreshTable("bannerTable");
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/banner/update", $bannerAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        $MB.n_success(r.msg);
                        $MB.refreshTable("bannerTable");
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#banner-add .btn-close").click(function () {
        closeModal();
    });

});

function closeModal() {
    $("#banner-add-button").attr("name", "save");
    validator.resetForm();
    $bannerAddForm.find("input[name='status']").prop("checked", true);
    $("#banner-add-modal-title").html('新增轮播图');
    $MB.closeAndRestModal("banner-add");

}
$('#uploadBanner').fileupload({
    autoUpload: true,//自动上传
    url: "/cms/filesUpload/banner",//ַ上传图片对应的地址
    dataType: 'json',
    done: function (e, data) {
        var oimage = data;
        $("#imgPath").val(oimage.result.msg.src);
      }
})
function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $bannerAddForm.validate({
        rules: {
            title: {
                required: true
            },
            imgPath: {
            	required: true
            }
        },
        errorPlacement: function (error, element) {
            if (element.is(":checkbox") || element.is(":radio")) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
            username: {
                required: icon + "请输入用户名",
                minlength: icon + "用户名长度3到10个字符",
                remote: icon + "用户名已经存在"
            }
        }
    });
}