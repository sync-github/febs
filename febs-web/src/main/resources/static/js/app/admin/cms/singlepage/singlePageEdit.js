function updateSinglePage() {
    var selected = $("#singlePageTable").bootstrapTable('getSelections');
    var selected_length = selected.length;
    if (!selected_length) {
        $MB.n_warning('请勾选需要修改的单页！');
        return;
    }
    if (selected_length > 1) {
        $MB.n_warning('一次只能修改一个单页！');
        return;
    }
    var singlePageId = selected[0].id;
    $.post(ctx + "admin/singlePage/getSinglePage", {"singlePageId": singlePageId}, function (r) {
        if (r.code === 0) {
            var $form = $('#singlePage-add');
            $form.modal();
            var singlePage = r.msg;
            $("#singlePage-add-modal-title").html('修改单页');
            $form.find("input[name='title']").val(singlePage.title);
            $form.find("input[name='slug']").val(singlePage.slug);
            $form.find("input[name='id']").val(singlePage.id);
            $form.find("input[name='keywords']").val(singlePage.keywords);
            $form.find("textarea[name='description']").val(singlePage.description);
            $form.find("textarea[name='content']").val(singlePage.content);
            $form.find("input[name='template']").val(singlePage.template);
            $form.find("input[name='seqNum']").val(singlePage.seqNum);
            $(":radio[name='status'][value='" + singlePage.status + "']").prop("checked", "checked");
            $(":radio[name='navShow'][value='" + singlePage.navShow + "']").prop("checked", "checked");
            CKEDITOR.instances.singlePageEditor.setData(singlePage.content);
            $("#singlePage-add-button").attr("name", "update");
        } else {
            $MB.n_danger(r.msg);
        }
    });
}