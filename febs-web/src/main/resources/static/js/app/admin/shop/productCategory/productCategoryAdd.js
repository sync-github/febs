var validator;
var $productCategoryAddForm = $("#productCategory-add-form");
$(function () {
    var options = {
    		single: true,
            width: '100%'
        };
    validateRule();
    createProductCategoryTree();
    $productCategoryAddForm.find("input[name='template']").val("productList.html");
    $("#productCategory-add .btn-save").click(function () {
        var name = $(this).attr("name");
        getProductCategory();
        validator = $productCategoryAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name === "save") {
                $.post(ctx + "admin/productCategory/add", $productCategoryAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        refresh();
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name === "update") {
                $.post(ctx + "admin/productCategory/update", $productCategoryAddForm.serialize(), function (r) {
                    if (r.code === 0) {
                        closeModal();
                        refresh();
                        $MB.n_success(r.msg);
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });

    $("#productCategory-add .btn-close").click(function () {
        closeModal();
    });

});
function closeModal() {
    $("#productCategory-add-button").attr("name", "save");
    $("#productCategory-add-modal-title").html('新增分类');
    validator.resetForm();
    $MB.closeAndRestModal("productCategory-add");
    $productCategoryAddForm.find("input[name='template']").val("productList.html");
    $MB.refreshJsTree("productCategoryTree", createProductCategoryTree());
}

function validateRule() {
    var icon = "<i class='zmdi zmdi-close-circle zmdi-hc-fw'></i> ";
    validator = $productCategoryAddForm.validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: icon + "请输入分类名称",
                minlength: icon + "分类名称长度3到10个字符",
                remote: icon + "该分类名称已经存在"
            }
        }
    });
}

function createProductCategoryTree() {
    $.post(ctx + "admin/productCategory/tree", {}, function (r) {
        if (r.code === 0) {
            var data = r.msg;
            $('#productCategoryTree').jstree({
                "core": {
                    'data': data.children,
                    'multiple': false
                },
                "state": {
                    "disabled": true
                },
                "checkbox": {
                    "three_state": false
                },
                "plugins": ["wholerow", "checkbox"]
            });
        } else {
            $MB.n_danger(r.msg);
        }
    })

}

function getProductCategory() {
    var ref = $('#productCategoryTree').jstree(true);
    var productCategoryIds = ref.get_checked();
    $("[name='parentId']").val(productCategoryIds[0]);
}
$('#uploadProCategory').fileupload({
    autoUpload: true,//自动上传
    url: "/cms/filesUpload/thumbnail",//ַ上传图片对应的地址
    dataType: 'json',
    done: function (e, data) {
        var oimage = data;
        $("#thumbnail").val(oimage.result.msg.src);
      }
})