-- MySQL dump 10.13  Distrib 8.0.20, for macos10.15 (x86_64)
--
-- Host: localhost    Database: febs
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `persistent_logins`
--

DROP TABLE IF EXISTS `persistent_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistent_logins`
--

LOCK TABLES `persistent_logins` WRITE;
/*!40000 ALTER TABLE `persistent_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `persistent_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_BLOB_TRIGGERS`
--

DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_BLOB_TRIGGERS`
--

LOCK TABLES `QRTZ_BLOB_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QRTZ_BLOB_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QRTZ_BLOB_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_CALENDARS`
--

DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_CALENDARS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_CALENDARS`
--

LOCK TABLES `QRTZ_CALENDARS` WRITE;
/*!40000 ALTER TABLE `QRTZ_CALENDARS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QRTZ_CALENDARS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_CRON_TRIGGERS`
--

DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(200) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_CRON_TRIGGERS`
--

LOCK TABLES `QRTZ_CRON_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QRTZ_CRON_TRIGGERS` DISABLE KEYS */;
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('MyScheduler','TASK_1','DEFAULT','0/1 * * * * ?','Asia/Shanghai'),('MyScheduler','TASK_11','DEFAULT','0/5 * * * * ?','Asia/Shanghai'),('MyScheduler','TASK_2','DEFAULT','0/10 * * * * ?','Asia/Shanghai'),('MyScheduler','TASK_3','DEFAULT','0/1 * * * * ?','Asia/Shanghai');
/*!40000 ALTER TABLE `QRTZ_CRON_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_FIRED_TRIGGERS`
--

DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_FIRED_TRIGGERS`
--

LOCK TABLES `QRTZ_FIRED_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QRTZ_FIRED_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QRTZ_FIRED_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_JOB_DETAILS`
--

DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_JOB_DETAILS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_JOB_DETAILS`
--

LOCK TABLES `QRTZ_JOB_DETAILS` WRITE;
/*!40000 ALTER TABLE `QRTZ_JOB_DETAILS` DISABLE KEYS */;
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('MyScheduler','TASK_1','DEFAULT',NULL,'com.febs.quartz.util.ScheduleJob','0','0','0','0',_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0a\�\�{pxt\0\r0/1 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0testt\0mrbirdt\0有参任务调度测试t\01x\0'),('MyScheduler','TASK_11','DEFAULT',NULL,'com.febs.quartz.util.ScheduleJob','0','0','0','0',_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0a\��Pxt\0\r0/5 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0test2pt\0测试异常t\01x\0'),('MyScheduler','TASK_2','DEFAULT',NULL,'com.febs.quartz.util.ScheduleJob','0','0','0','0',_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0a\�=�xt\00/10 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0test1pt\0无参任务调度测试t\01x\0'),('MyScheduler','TASK_3','DEFAULT',NULL,'com.febs.quartz.util.ScheduleJob','0','0','0','0',_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0aҺ��xt\0\r0/1 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0testt\0hello worldt\0+有参任务调度测试,每隔一秒触发t\01x\0');
/*!40000 ALTER TABLE `QRTZ_JOB_DETAILS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_LOCKS`
--

DROP TABLE IF EXISTS `QRTZ_LOCKS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_LOCKS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_LOCKS`
--

LOCK TABLES `QRTZ_LOCKS` WRITE;
/*!40000 ALTER TABLE `QRTZ_LOCKS` DISABLE KEYS */;
INSERT INTO `QRTZ_LOCKS` VALUES ('MyScheduler','STATE_ACCESS'),('MyScheduler','TRIGGER_ACCESS');
/*!40000 ALTER TABLE `QRTZ_LOCKS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_PAUSED_TRIGGER_GRPS`
--

DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_PAUSED_TRIGGER_GRPS`
--

LOCK TABLES `QRTZ_PAUSED_TRIGGER_GRPS` WRITE;
/*!40000 ALTER TABLE `QRTZ_PAUSED_TRIGGER_GRPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QRTZ_PAUSED_TRIGGER_GRPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_SCHEDULER_STATE`
--

DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_SCHEDULER_STATE`
--

LOCK TABLES `QRTZ_SCHEDULER_STATE` WRITE;
/*!40000 ALTER TABLE `QRTZ_SCHEDULER_STATE` DISABLE KEYS */;
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('MyScheduler','wangtaodeMacBook-Pro.local1592529111455',1592529416462,15000);
/*!40000 ALTER TABLE `QRTZ_SCHEDULER_STATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_SIMPLE_TRIGGERS`
--

DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_SIMPLE_TRIGGERS`
--

LOCK TABLES `QRTZ_SIMPLE_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QRTZ_SIMPLE_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QRTZ_SIMPLE_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_SIMPROP_TRIGGERS`
--

DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_SIMPROP_TRIGGERS`
--

LOCK TABLES `QRTZ_SIMPROP_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QRTZ_SIMPROP_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QRTZ_SIMPROP_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QRTZ_TRIGGERS`
--

DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `QRTZ_TRIGGERS` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QRTZ_TRIGGERS`
--

LOCK TABLES `QRTZ_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QRTZ_TRIGGERS` DISABLE KEYS */;
INSERT INTO `QRTZ_TRIGGERS` VALUES ('MyScheduler','TASK_1','DEFAULT','TASK_1','DEFAULT',NULL,1580596816000,-1,5,'PAUSED','CRON',1580596816000,0,NULL,2,_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0a\�\�{pxt\0\r0/1 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0testt\0mrbirdt\0有参任务调度测试t\01x\0'),('MyScheduler','TASK_11','DEFAULT','TASK_11','DEFAULT',NULL,1580596820000,-1,5,'PAUSED','CRON',1580596816000,0,NULL,2,_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0a\��Pxt\0\r0/5 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0test2t\0\0t\0测试异常t\01x\0'),('MyScheduler','TASK_2','DEFAULT','TASK_2','DEFAULT',NULL,1580596820000,-1,5,'PAUSED','CRON',1580596816000,0,NULL,2,_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0a\�=�xt\00/10 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0test1pt\0无参任务调度测试t\01x\0'),('MyScheduler','TASK_3','DEFAULT','TASK_3','DEFAULT',NULL,1580596816000,-1,5,'PAUSED','CRON',1580596816000,0,NULL,2,_binary '�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rJOB_PARAM_KEYsr\0cc.mrbird.quartz.domain.Job\0\0\0\0\0\0\0\0L\0beanNamet\0Ljava/lang/String;L\0\ncreateTimet\0Ljava/util/Date;L\0cronExpressionq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0\nmethodNameq\0~\0	L\0paramsq\0~\0	L\0remarkq\0~\0	L\0statusq\0~\0	xpt\0testTasksr\0java.util.Datehj�KYt\0\0xpw\0\0aҺ��xt\0\r0/1 * * * * ?sr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0testt\0hello worldt\0+有参任务调度测试,每隔一秒触发t\01x\0');
/*!40000 ALTER TABLE `QRTZ_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_article`
--

DROP TABLE IF EXISTS `t_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_ids` varchar(100) DEFAULT NULL COMMENT '分类',
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(120) DEFAULT NULL COMMENT 'slug',
  `meta_description` varchar(100) DEFAULT NULL COMMENT 'seo description',
  `meta_keywords` varchar(100) DEFAULT NULL COMMENT 'seo keywords',
  `author` varchar(45) DEFAULT NULL COMMENT '作者',
  `thumbnail` varchar(150) DEFAULT NULL COMMENT '缩略图',
  `seq_num` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `template` varchar(100) DEFAULT NULL COMMENT '文章模板',
  `summary` text COMMENT '摘要',
  `content` text,
  `tags` varchar(100) DEFAULT NULL COMMENT '标签冗余字段',
  `view_count` int(11) DEFAULT '0' COMMENT '阅读次数',
  `article_status` char(1) DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `is_recommend` char(1) DEFAULT NULL COMMENT '是否推荐 0否 1是',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_article`
--

LOCK TABLES `t_article` WRITE;
/*!40000 ALTER TABLE `t_article` DISABLE KEYS */;
INSERT INTO `t_article` VALUES (1,'22','wwer写经验交流材料的技巧全在这了！','myarticletest','wwer写经验珍珠是用黑糖熬制的，要熬整整四个小时才行，每熬一锅黑糖只能做出40杯奶茶，好味，是值得等待的。','sssee','admin','/layuiNetCompany/res/static/img/news_img1.jpg',2,1,'article.html','找老婆要找爱发脾气的女人。永远不会发脾气的女人就如同一杯白开水，解渴，却无味。而发脾气的女人正如烈酒般，刺激而令人无法忘怀。\r\n我的内容测试输入。','<p>fdsafdsafdsa</p>\r\n','技术交流,工程心德',0,'1',NULL,'2018-01-04 15:42:29'),(2,'22','33脾气fdsfds好读读这篇文章','tes23','ffdsfds','fdfdsa','admin','/layuiNetCompany/res/static/img/news_img1.jpg',2,1,'article.html','看不到您的原稿，这样对空发议论，估计对您的指导性是不大的。建议您将原稿贴出来，好让老师们针对指导。这里简单给出意见：','<p>aaaa</p>','',10,'1',NULL,'2018-01-04 15:42:29'),(3,'21,22','22脾气不好的妈妈好好读读这篇文章','tes','cccc','aaa','admin','/layuiNetCompany/res/static/img/news_img1.jpg',3,1,'article.html','在日常的工作中，不知道大家有没有遇到以下这些问题：面对这样的问题，我养成了一个习惯就是每天写工作计划。想在此与大家分享，希望对你们有所帮助','<table style=\"border-collapse:collapse; width:100%\"> \n <tbody> \n  <tr> \n   <td style=\"width:33.3333%\">ttty</td> \n   <td style=\"width:33.3333%\">l0000</td> \n   <td style=\"width:33.3333%\">&nbsp;</td> \n  </tr> \n  <tr> \n   <td style=\"width:33.3333%\">ioio</td> \n   <td style=\"width:33.3333%\">&nbsp;</td> \n   <td style=\"width:33.3333%\">555</td> \n  </tr> \n </tbody> \n</table> \n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p> \n<p>uiui lllll &nbsp; oo98</p> \n<p>piop &nbsp; &nbsp; jojlk</p> \n<p>&nbsp; &nbsp;gjlgljkhk</p> \n<p>aa &nbsp; &nbsp; &nbsp; fff &nbsp; dfdsa &nbsp; &nbsp; ddd ssss &nbsp; ddd ss &nbsp;</p> \n<p>eeeeeegggghhhffff</p> \n<p>aafdsafdsafdsafdsa556788¤</p>','',0,'1',NULL,'2020-02-05 10:40:17'),(4,'4','写经验交流材料的技巧全在这了！',NULL,'写经验交流材料的技巧全在这了！','写经验交流材料的技巧全在这了！','admin','/layuiNetCompany/res/static/img/news_img1.jpg',4,1,'article.html','有些人再好，如果他不属于你，就什么用也没有。别再为一个得不到的人辗转难眠，你熬夜到天亮，对他来说也无关痛痒。','<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！&nbsp;</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！&nbsp;</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！&nbsp;</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！&nbsp;</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！&nbsp;</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">写经验交流材料的技巧全在这了！&nbsp;</a></p>',NULL,0,'1',NULL,'2020-02-13 00:43:11'),(5,'4','建议您将原稿贴出来，好让老师们针对指导',NULL,'建议您将原稿贴出来，好让老师们针对指导','建议您将原稿贴出来，好让老师们针对指导','admin','/layuiNetCompany/res/static/img/news_img1.jpg',5,1,'article.html','一个母亲要是控制不了自己的情绪，会发生多可怕的事情？去年一个9岁的男孩，因为弄丢了手机，遭受了亲妈捆绑殴打一整晚，第二天再也没能睁开眼睛。','<p>&nbsp;</p>\r\n<p><span style=\"color: #777777; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\">建议您将原稿贴出来，好让老师们针对指导</span></p>',NULL,0,'1',NULL,'2020-02-13 00:44:20'),(6,'4','经验分享：我是如何做好每日计划的',NULL,'经验分享：我是如何做好每日计划的','经验分享：我是如何做好每日计划的','admin','/layuiNetCompany/res/static/img/news_img1.jpg',6,1,'article.html','孩子正处于长身体的重要阶段，对于营养的需求自然不小。很多家长怕孩子半夜饿，喜欢在睡前补充一些食物。','<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>经验分享：我是如何做好每日计划的</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">经验分享：我是如何做好每日计划的</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">经验分享：我是如何做好每日计划的</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">经验分享：我是如何做好每日计划的</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">经验分享：我是如何做好每日计划的</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">经验分享：我是如何做好每日计划的</a></strong></p>',NULL,11,'1',NULL,'2020-02-13 00:45:30'),(7,'4','养女儿，一定要让她漂亮！',NULL,'养女儿，一定要让她漂亮！','养女儿，一定要让她漂亮！','admin','/layuiNetCompany/res/static/img/news_img1.jpg',7,1,'article.html','闹市街头，一位爸爸领着一对双胞胎女儿吸引住了我的目光。','<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>养女儿，一定要让她漂亮！</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">养女儿，一定要让她漂亮！</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">养女儿，一定要让她漂亮！</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">养女儿，一定要让她漂亮！</a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br></a></strong><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; display: inline !important;\" rel=\"nofollow\">养女儿，一定要让她漂亮！</a></strong></p>',NULL,0,'1',NULL,'2020-02-13 00:47:32'),(8,'4','见好友在同一趟高铁 粗心男子“串门”聊天',NULL,'见好友在同一趟高铁 粗心男子“串门”聊天','见好友在同一趟高铁 粗心男子“串门”聊天','admin','/layuiNetCompany/res/static/img/news_img1.jpg',8,1,'article.html','两个女儿五六岁，长得像天使一般动人。让人遗憾的是，家长给他们穿的衣服---大妈们买菜或做饭时穿的长袍式的家居服','<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a><a style=\"color: #555555; text-decoration-line: none; outline: 0px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; font-weight: bold; background-color: #ffffff; display: inline !important;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a></p>\r\n<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a></p>\r\n<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">见好友在同一趟高铁 粗心男子“串门”聊天</a></p>',NULL,0,'1',NULL,'2020-02-13 00:48:35'),(9,'4','写经验交流材料的技巧全在这了！',NULL,'写经验交流材料的技巧全在这了！','写经验交流材料的技巧全在这了！','admin','/layuiNetCompany/res/static/img/news_img1.jpg',9,1,'article.html','出门在外若发现好友在同一趟高铁邂逅，你会如何做？近日，一男子上高铁后，打电话给一位久未联系好友，竟然得知出行乘坐同一趟车。于是，该男子兴奋地“串门”。','<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>写经验交流材料的技巧全在这了！</a></strong></p>\r\n<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>写经验交流材料的技巧全在这了！</a></strong></p>\r\n<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>写经验交流材料的技巧全在这了！</a></strong></p>\r\n<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>写经验交流材料的技巧全在这了！</a></strong></p>\r\n<p><strong style=\"color: #000000; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; background-color: #ffffff;\"><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;\" rel=\"nofollow\"><br>写经验交流材料的技巧全在这了！</a></strong></p>',NULL,0,'1',NULL,'2020-02-13 00:49:34'),(10,'21','脾气不好的妈妈好好读读这篇文章','piqbhdmmhhddzpwz','脾气不好的妈妈好好读读这篇文章','脾气不好的妈妈好好读读这篇文章','admin','/layuiNetCompany/res/static/img/news_img1.jpg',1,1,'article.html','找老婆要找爱发脾气的女人。永远不会发脾气的女人就如同一杯白开水，解渴，却无味。而发脾气的女人正如烈酒般，刺激而令人无法忘怀。','<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:10px\">北极光的制作，需要300g葡萄来完成，要选用最新鲜的葡萄，才能做出最灿烂的北极光。满满一瓶葡萄，看着就让人倍感满足。喝之前，要先摇9下，才能喝出最好的果味。晨曦的寓意是，清晨的阳光。要用到一整颗百香果的晨曦，喝起来酸酸甜甜，果味浓郁。晨曦喝起来果味极浓，不仅仅有百香果，还有芒果、橙汁...的味道，十分清新，彷佛夏日的一抹凉风！</span></p>\r\n','脾气,性格,育儿',13,'1',NULL,'2020-02-13 00:50:32'),(11,'4','一直在你身边对你好，你却没有发现。',NULL,'一直在你身边对你好，你却没有发现。','一直在你身边对你好，你却没有发现。','admin','/layuiNetCompany/res/static/img/news_img1.jpg',2,1,'article.html','看不到您的原稿，这样对空发议论，估计对您的指导性是不大的。建议您将原稿贴出来，好让老师们针对指导。这里简单给出意见：','<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">一直在你身边对你好，你却没有发现。</a></p>\r\n<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">一直在你身边对你好，你却没有发现。</a></p>\r\n<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">一直在你身边对你好，你却没有发现。</a></p>\r\n<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">一直在你身边对你好，你却没有发现。</a></p>\r\n<p><a style=\"color: #555555; text-decoration-line: none; outline: 0px; display: block; font-size: 20px; line-height: 30px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; font-family: \'Helvetica Neue\', Helvetica, \'PingFang SC\', Tahoma, Arial, sans-serif; font-weight: bold; background-color: #ffffff;\" rel=\"nofollow\">一直在你身边对你好，你却没有发现。</a></p>',NULL,0,'1',NULL,'2020-02-13 00:51:23'),(12,'20','名牌工厂店1','mingpghd','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','ff','admin','/layuiNetCompany/res/static/img/case1.jpg',3,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p><span style=\"font-size:14px\"><span style=\"color:#e74c3c\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。一家工厂企业的商品展示网站，主</span><span style=\"color:#2c3e50\">要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></span></p>\r\n','',121,'1',NULL,'2020-02-13 08:17:13'),(13,'5','名牌工厂店2',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','dd','admin','/layuiNetCompany/res/static/img/case2.jpg',4,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(14,'5','名牌工厂店3',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','ee','admin','/layuiNetCompany/res/static/img/case3.jpg',5,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(15,'5','名牌工厂店4',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','aa','admin','/layuiNetCompany/res/static/img/case4.jpg',6,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(16,'5','名牌工厂店5',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','ff','admin','/layuiNetCompany/res/static/img/case5.jpg',7,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(17,'21','名牌工厂店6','','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','aa','admin','/layuiNetCompany/res/static/img/case6.jpg',8,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p>一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color:#000000\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color:#000000\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color:#000000\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color:#000000\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color:#000000\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>\r\n','',2,'0',NULL,'2020-02-13 08:17:13'),(18,'5','名牌工厂店7',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','sd','admin','/layuiNetCompany/res/static/img/case7.jpg',8,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(19,'5','名牌工厂店8',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','ffd','admin','/layuiNetCompany/res/static/img/case8.jpg',9,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(20,'5','名牌工厂店9',NULL,'一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','dfd','admin','/layuiNetCompany/res/static/img/case9.jpg',11,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p style=\"margin-top: 0px; margin-bottom: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Helvetica; color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。<span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span><span style=\"color: #000000;\">一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。</span></p>',NULL,0,'1',NULL,'2020-02-13 08:17:13'),(22,'22','同步数据','jingcaizhuanti','精彩专题','afdsafd','admin','/essence/img/bg-img/blog2.jpg',1,1,'article.html','一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。','<p>精彩专题精彩专题精彩专题精彩专题f我的CKEDITOR 的同步数据dsafdsafdsafdsa</p>\r\n','',1,'0',NULL,'2020-02-14 05:03:47'),(25,'1','测试tag标签文章',NULL,'开始测试 的弱电音响','tag,音响工程','admin','/essence/img/bg-img/blog1.jpg',2,1,'article.html','开始测试 的弱电音响开始测试 的弱电音响','<p>开始测试 的弱电音响开始测试 的弱电音响开始测试 的弱电音响</p>\r\n','音响,弱电工程',NULL,'1',NULL,'2020-03-13 10:42:15'),(31,'20','我的前台','wodqt','我的前台','我的前台','admin','/essence/img/bg-img/blog3.jpg',NULL,1,'article.html','','<p>我的前台我的前台我的前台我的前台我的前台我的前台我的前台我的前台发</p>\r\n','前台,客户',NULL,'1',NULL,'2020-05-12 21:48:24');
/*!40000 ALTER TABLE `t_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_article_category_mapping`
--

DROP TABLE IF EXISTS `t_article_category_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_article_category_mapping` (
  `article_id` int(11) unsigned NOT NULL COMMENT '文章ID',
  `category_id` int(11) unsigned NOT NULL COMMENT '分类ID',
  PRIMARY KEY (`article_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文章和分类的多对多关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_article_category_mapping`
--

LOCK TABLES `t_article_category_mapping` WRITE;
/*!40000 ALTER TABLE `t_article_category_mapping` DISABLE KEYS */;
INSERT INTO `t_article_category_mapping` VALUES (1,22),(2,22),(3,21),(3,22),(4,4),(5,4),(6,4),(7,4),(8,4),(9,4),(10,21),(11,4),(12,20),(13,5),(14,5),(15,5),(16,5),(17,21),(18,5),(19,5),(20,5),(22,22),(25,1),(31,20);
/*!40000 ALTER TABLE `t_article_category_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_article_comment`
--

DROP TABLE IF EXISTS `t_article_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_article_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` int(11) unsigned DEFAULT NULL COMMENT '回复的评论ID',
  `article_id` int(11) unsigned DEFAULT NULL COMMENT '评论的内容ID',
  `title` varchar(100) DEFAULT NULL COMMENT '文章标题',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '评论的用户ID',
  `author` varchar(128) DEFAULT NULL COMMENT '评论的作者',
  `content` text COMMENT '评论的内容',
  `reply_count` int(11) unsigned DEFAULT '0' COMMENT '评论的回复数量',
  `seq_num` int(11) DEFAULT '0' COMMENT '排序编号，常用语置顶等',
  `vote_up` int(11) unsigned DEFAULT '0' COMMENT '“顶”的数量',
  `vote_down` int(11) unsigned DEFAULT '0' COMMENT '“踩”的数量',
  `comment_status` char(1) DEFAULT NULL COMMENT '评论的状态',
  `modify_uid` int(11) DEFAULT NULL,
  `modify_username` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '评论的时间',
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_id` (`article_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文章评论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_article_comment`
--

LOCK TABLES `t_article_comment` WRITE;
/*!40000 ALTER TABLE `t_article_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_article_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_article_tag`
--

DROP TABLE IF EXISTS `t_article_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_article_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(45) DEFAULT NULL COMMENT '标签名称',
  `template` varchar(100) DEFAULT NULL COMMENT '标签列表页模板',
  `counts` int(11) DEFAULT '0' COMMENT '文章数量',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_article_tag`
--

LOCK TABLES `t_article_tag` WRITE;
/*!40000 ALTER TABLE `t_article_tag` DISABLE KEYS */;
INSERT INTO `t_article_tag` VALUES (1,'java','tagArticleList.html',3,'2020-03-13 02:50:40'),(2,'音响','tagArticleList.html',-3,'2020-03-13 09:58:53'),(3,'弱电','tagArticleList.html',0,'2020-03-13 09:58:53'),(5,'技术交流','tagArticleList.html',-5,'2020-03-13 10:51:07'),(6,'工程心德','tagArticleList.html',-5,'2020-03-13 10:51:07'),(10,'脾气','tagArticleList.html',-1,'2020-04-20 19:37:55'),(11,'性格','tagArticleList.html',-1,'2020-04-20 19:37:55'),(12,'育儿','tagArticleList.html',-1,'2020-04-20 19:37:55'),(13,'','tagArticleList.html',-20,'2020-04-23 01:43:42'),(14,'前台','tagArticleList.html',-1,'2020-05-12 21:48:24'),(15,'客户','tagArticleList.html',-1,'2020-05-12 21:48:24');
/*!40000 ALTER TABLE `t_article_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_article_tag_mapping`
--

DROP TABLE IF EXISTS `t_article_tag_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_article_tag_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_article_tag_mapping`
--

LOCK TABLES `t_article_tag_mapping` WRITE;
/*!40000 ALTER TABLE `t_article_tag_mapping` DISABLE KEYS */;
INSERT INTO `t_article_tag_mapping` VALUES (21,25,2),(22,25,4),(62,10,10),(63,10,11),(64,10,12),(66,1,5),(67,1,6),(70,31,14),(71,31,15),(73,12,13),(74,17,13),(76,22,13),(85,2,13),(86,3,13);
/*!40000 ALTER TABLE `t_article_tag_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_banner`
--

DROP TABLE IF EXISTS `t_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL COMMENT 'banner名称',
  `img_path` varchar(100) DEFAULT NULL COMMENT '图片路径',
  `open_url` varchar(100) DEFAULT NULL COMMENT '打开链接',
  `open_target` varchar(45) DEFAULT NULL COMMENT '打开方式',
  `description` text,
  `seq_num` int(10) DEFAULT NULL COMMENT '序号',
  `article_status` char(1) DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_banner`
--

LOCK TABLES `t_banner` WRITE;
/*!40000 ALTER TABLE `t_banner` DISABLE KEYS */;
INSERT INTO `t_banner` VALUES (1,'网络公司1','/demoimgs/img_fjords_wide.jpg','https://www.baidu.com','_blank','完美前端体验',1,'1','2020-02-13 00:45:30'),(2,'网络公司2','/demoimgs/img_mountains_wide.jpg','https://www.163.com','_blank','完美前端体验',2,'1','2020-02-13 00:45:30'),(4,'专题链接','/demoimgs/img_nature_wide.jpg','http://www.baidu.com','_blank','专题链接专题链接专题链接',3,'1','2020-06-13 11:06:27');
/*!40000 ALTER TABLE `t_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_category`
--

DROP TABLE IF EXISTS `t_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `slug` varchar(120) DEFAULT NULL COMMENT 'slug',
  `seq_num` int(11) DEFAULT '0' COMMENT '排序',
  `user_id` int(11) DEFAULT NULL COMMENT '创建用户ID',
  `meta_keywords` varchar(100) DEFAULT NULL COMMENT '分类描述关键字',
  `meta_description` varchar(100) DEFAULT NULL COMMENT '分类描述',
  `category_status` char(1) DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `nav_show` char(1) DEFAULT NULL COMMENT '是否显示在导航上 状态 0否 1是',
  `template` varchar(100) DEFAULT NULL COMMENT '分类所对应模板',
  `counts` int(11) DEFAULT NULL COMMENT '文章数量',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`,`create_time`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_category`
--

LOCK TABLES `t_category` WRITE;
/*!40000 ALTER TABLE `t_category` DISABLE KEYS */;
INSERT INTO `t_category` VALUES (14,0,'新闻','xinwenzhongxin',4,1,'博客','博客内容列表','1','1','articleList.html',NULL,'2020-03-03 06:58:31'),(20,14,'公司动态','gongsidongtai',1,1,'公司动态keywords','公司动态descriptions','1','1','articleList.html',NULL,'2020-03-03 07:04:49'),(21,14,'行业动态','hengyedongtai',2,1,'行业动态keywords','行业动态descriptions','1','1','articleList.html',NULL,'2020-03-03 07:05:30'),(22,14,'精彩专题','jingcaizhuanti',3,1,'精彩专题keywords','精彩专题descriptions','1','1','articleList.html',NULL,'2020-03-03 07:06:29');
/*!40000 ALTER TABLE `t_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_coupon`
--

DROP TABLE IF EXISTS `t_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_coupon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '例如：无门槛50元优惠券 | 单品最高减2000元''',
  `icon` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '1满减券  2叠加满减券  3无门槛券  ',
  `with_amount` decimal(10,2) DEFAULT NULL COMMENT '满多少金额',
  `with_award` tinyint(1) DEFAULT NULL COMMENT '是否是推广奖励券',
  `with_owner` tinyint(1) DEFAULT NULL COMMENT '是不是只有领取人可用，如果不是，领取人可以随便给其他人用',
  `amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '优惠券金额',
  `quota` int(11) unsigned NOT NULL COMMENT '配额：发券数量',
  `take_count` int(11) unsigned DEFAULT '0' COMMENT '已领取的优惠券数量',
  `used_count` int(11) unsigned DEFAULT '0' COMMENT '已使用的优惠券数量',
  `start_time` datetime DEFAULT NULL COMMENT '发放开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '发放结束时间',
  `valid_type` tinyint(2) DEFAULT NULL COMMENT '时效:1绝对时效（XXX-XXX时间段有效）  2相对时效（领取后N天有效）',
  `valid_start_time` datetime DEFAULT NULL COMMENT '使用开始时间',
  `valid_end_time` datetime DEFAULT NULL COMMENT '使用结束时间',
  `valid_days` int(11) DEFAULT NULL COMMENT '自领取之日起有效天数',
  `create_user_id` int(11) unsigned DEFAULT NULL COMMENT '创建用户',
  `cou_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兑换码(可选，如果有值说明支持兑换码领取此类型优惠券)',
  `products_type` smallint(6) DEFAULT NULL COMMENT '商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。',
  `products_value` varchar(1023) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '[]' COMMENT '商品限制值，products_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `create_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='优惠券类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_coupon`
--

LOCK TABLES `t_coupon` WRITE;
/*!40000 ALTER TABLE `t_coupon` DISABLE KEYS */;
INSERT INTO `t_coupon` VALUES (1,'50元通用券','',3,0.00,NULL,NULL,50.00,15,0,0,NULL,NULL,NULL,'2020-05-26 04:55:00','2020-05-28 08:50:00',NULL,1,'1','',0,NULL,NULL,'2020-05-26 16:03:14','2020-05-26 16:03:14');
/*!40000 ALTER TABLE `t_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_coupon_user`
--

DROP TABLE IF EXISTS `t_coupon_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_coupon_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) unsigned DEFAULT NULL COMMENT '类型ID',
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '优惠券标题',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '领取用户ID',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `cou_status` tinyint(2) DEFAULT NULL COMMENT '状态 1 有人领取、正常使用  2 未有人领取不能使用  3 已经使用，不能被再次使用  9 已经被认为标识不可用',
  `used_order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `order_ns` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单号',
  `user_payment_id` int(11) DEFAULT NULL COMMENT '支付的ID',
  `use_time` datetime DEFAULT NULL COMMENT '使用时间',
  `get_time` datetime DEFAULT NULL COMMENT '领取时间',
  `send_uid` int(11) DEFAULT NULL COMMENT '如果是由后台管理员发放，则记录下由哪位管理员发放的。前台用户自己领取的，此字段将为空。',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间，创建时可能不会有人领取',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='优惠券领取记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_coupon_user`
--

LOCK TABLES `t_coupon_user` WRITE;
/*!40000 ALTER TABLE `t_coupon_user` DISABLE KEYS */;
INSERT INTO `t_coupon_user` VALUES (5,1,'50元通用券',1,'admin',2,24,'OC464594334789152768',NULL,'2020-05-31 14:13:07',NULL,1,'2020-05-27 07:32:12'),(6,1,'50元通用券',2,'scott',1,NULL,NULL,NULL,NULL,NULL,1,'2020-05-27 07:32:12'),(7,1,'50元通用券',173,'wtsoftware',2,NULL,NULL,NULL,NULL,NULL,1,'2020-05-27 07:32:12'),(8,1,'50元通用券',1,'admin',2,23,NULL,NULL,'2020-05-30 15:47:17','2020-05-30 15:45:53',1,'2020-05-30 15:45:53');
/*!40000 ALTER TABLE `t_coupon_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_delivery_company`
--

DROP TABLE IF EXISTS `t_delivery_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_delivery_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT '快递公司名称',
  `telphone` varchar(45) DEFAULT NULL COMMENT '联系电话',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='快递公司维护';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_delivery_company`
--

LOCK TABLES `t_delivery_company` WRITE;
/*!40000 ALTER TABLE `t_delivery_company` DISABLE KEYS */;
INSERT INTO `t_delivery_company` VALUES (1,'顺丰快递','90044','2020-02-04 07:08:58'),(2,'圆通快递','89434','2020-02-04 07:08:58'),(3,'A物流公司','2345','2020-02-04 07:08:58');
/*!40000 ALTER TABLE `t_delivery_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_dept`
--

DROP TABLE IF EXISTS `t_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_dept` (
  `DEPT_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级部门ID',
  `DEPT_NAME` varchar(100) NOT NULL COMMENT '部门名称',
  `ORDER_NUM` bigint(20) DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`DEPT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_dept`
--

LOCK TABLES `t_dept` WRITE;
/*!40000 ALTER TABLE `t_dept` DISABLE KEYS */;
INSERT INTO `t_dept` VALUES (1,0,'开发部',NULL,'2018-01-04 15:42:26'),(2,1,'开发一部',NULL,'2018-01-04 15:42:34'),(3,1,'开发二部',NULL,'2018-01-04 15:42:29'),(4,0,'市场部',NULL,'2018-01-04 15:42:36'),(5,0,'人事部',NULL,'2018-01-04 15:42:32'),(6,0,'测试部',NULL,'2018-01-04 15:42:38'),(11,0,'外部注册',NULL,'2020-04-27 00:43:58');
/*!40000 ALTER TABLE `t_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_dict`
--

DROP TABLE IF EXISTS `t_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_dict` (
  `DICT_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典ID',
  `KEYY` bigint(20) NOT NULL COMMENT '键',
  `VALUEE` varchar(100) NOT NULL COMMENT '值',
  `FIELD_NAME` varchar(100) NOT NULL COMMENT '字段名称',
  `TABLE_NAME` varchar(100) NOT NULL COMMENT '表名',
  PRIMARY KEY (`DICT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_dict`
--

LOCK TABLES `t_dict` WRITE;
/*!40000 ALTER TABLE `t_dict` DISABLE KEYS */;
INSERT INTO `t_dict` VALUES (1,0,'男','ssex','t_user'),(2,1,'女','ssex','t_user'),(3,2,'保密','ssex','t_user'),(4,1,'有效','status','t_user'),(5,0,'锁定','status','t_user'),(6,0,'菜单','type','t_menu'),(7,1,'按钮','type','t_menu'),(30,0,'正常','status','t_job'),(31,1,'暂停','status','t_job'),(32,0,'成功','status','t_job_log'),(33,1,'失败','status','t_job_log');
/*!40000 ALTER TABLE `t_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_friend_links`
--

DROP TABLE IF EXISTS `t_friend_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_friend_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `url_address` varchar(100) DEFAULT NULL COMMENT '链接地址',
  `thumbnail` varchar(100) DEFAULT NULL COMMENT '链接图片',
  `seq_num` int(11) DEFAULT NULL,
  `target` varchar(45) DEFAULT NULL COMMENT '链接打开方式 _blank和 _self',
  `link_states` char(1) DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `user_id` varchar(45) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_friend_links`
--

LOCK TABLES `t_friend_links` WRITE;
/*!40000 ALTER TABLE `t_friend_links` DISABLE KEYS */;
INSERT INTO `t_friend_links` VALUES (1,'百度','http://www.baidu.com','/upload/case5-1583036477890.jpg',1,'_self','1','1','2017-12-27 16:47:13'),(2,'网易','http://www.163.com','/upload/b2-1587571537697.jpg',2,'_self','1','1','2020-02-23 03:58:08'),(3,'新浪','http://www.sina.com','/upload/case1-1583036497837.jpg',3,'_self','1','1','2020-02-23 05:48:39');
/*!40000 ALTER TABLE `t_friend_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_job`
--

DROP TABLE IF EXISTS `t_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_job` (
  `JOB_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `BEAN_NAME` varchar(100) NOT NULL COMMENT 'spring bean名称',
  `METHOD_NAME` varchar(100) NOT NULL COMMENT '方法名',
  `PARAMS` varchar(200) DEFAULT NULL COMMENT '参数',
  `CRON_EXPRESSION` varchar(100) NOT NULL COMMENT 'cron表达式',
  `STATUS` char(2) NOT NULL COMMENT '任务状态  0：正常  1：暂停',
  `REMARK` varchar(200) DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`JOB_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_job`
--

LOCK TABLES `t_job` WRITE;
/*!40000 ALTER TABLE `t_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_job_log`
--

DROP TABLE IF EXISTS `t_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_job_log` (
  `LOG_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `JOB_ID` bigint(20) NOT NULL COMMENT '任务id',
  `BEAN_NAME` varchar(100) NOT NULL COMMENT 'spring bean名称',
  `METHOD_NAME` varchar(100) NOT NULL COMMENT '方法名',
  `PARAMS` varchar(200) DEFAULT NULL COMMENT '参数',
  `STATUS` char(2) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `ERROR` text COMMENT '失败信息',
  `TIMES` decimal(11,0) DEFAULT NULL COMMENT '耗时(单位：毫秒)',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`LOG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2496 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_job_log`
--

LOCK TABLES `t_job_log` WRITE;
/*!40000 ALTER TABLE `t_job_log` DISABLE KEYS */;
INSERT INTO `t_job_log` VALUES (2448,3,'testTask','test','hello world','0',NULL,0,'2018-03-20 15:31:50'),(2449,3,'testTask','test','hello world','0',NULL,1,'2018-03-20 15:31:51'),(2450,3,'testTask','test','hello world','0',NULL,2,'2018-03-20 15:31:52'),(2451,3,'testTask','test','hello world','0',NULL,0,'2018-03-20 15:31:53'),(2452,3,'testTask','test','hello world','0',NULL,2,'2018-03-20 15:31:54');
/*!40000 ALTER TABLE `t_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_log`
--

DROP TABLE IF EXISTS `t_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '操作用户',
  `OPERATION` text COMMENT '操作内容',
  `TIME` decimal(11,0) DEFAULT NULL COMMENT '耗时',
  `METHOD` text COMMENT '操作方法',
  `PARAMS` text COMMENT '方法参数',
  `IP` varchar(64) DEFAULT NULL COMMENT '操作者IP',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `location` varchar(50) DEFAULT NULL COMMENT '操作地点',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10994 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_log`
--

LOCK TABLES `t_log` WRITE;
/*!40000 ALTER TABLE `t_log` DISABLE KEYS */;
INSERT INTO `t_log` VALUES (10671,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-12 12:54:33','内网IP|0|0|内网IP|内网IP'),(10672,'admin','获取用户信息',4,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-12 12:54:35','内网IP|0|0|内网IP|内网IP'),(10673,'admin','获取字典信息',3,'com.febs.web.controller.admin.system.DictController.index()','','127.0.0.1','2020-06-12 12:54:41','内网IP|0|0|内网IP|内网IP'),(10674,'admin','获取定时任务信息',3,'com.febs.web.controller.quartz.JobController.index()','','127.0.0.1','2020-06-13 00:37:52','内网IP|0|0|内网IP|内网IP'),(10675,'admin','获取调度日志信息',3,'com.febs.web.controller.quartz.JobLogController.index()','','127.0.0.1','2020-06-13 00:37:54','内网IP|0|0|内网IP|内网IP'),(10776,'admin','获取用户信息',19,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-15 06:14:43','内网IP|0|0|内网IP|内网IP'),(10777,'admin','获取角色信息',0,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 06:14:45','内网IP|0|0|内网IP|内网IP'),(10778,'admin','获取系统所有URL',21,'com.febs.web.controller.admin.system.MenuController.getAllUrl()','','127.0.0.1','2020-06-15 06:14:47','内网IP|0|0|内网IP|内网IP'),(10779,'admin','获取部门信息',0,'com.febs.web.controller.admin.system.DeptController.index()','','127.0.0.1','2020-06-15 06:14:47','内网IP|0|0|内网IP|内网IP'),(10780,'admin','获取用户信息',5,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-15 06:14:55','内网IP|0|0|内网IP|内网IP'),(10781,'admin','获取角色信息',1,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 06:15:15','内网IP|0|0|内网IP|内网IP'),(10782,'admin','获取部门信息',0,'com.febs.web.controller.admin.system.DeptController.index()','','127.0.0.1','2020-06-15 06:15:17','内网IP|0|0|内网IP|内网IP'),(10783,'admin','获取字典信息',3,'com.febs.web.controller.admin.system.DictController.index()','','127.0.0.1','2020-06-15 06:15:19','内网IP|0|0|内网IP|内网IP'),(10784,'admin','获取在线用户信息',3,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:15:23','内网IP|0|0|内网IP|内网IP'),(10785,'admin','获取定时任务信息',4,'com.febs.web.controller.quartz.JobController.index()','','127.0.0.1','2020-06-15 06:15:27','内网IP|0|0|内网IP|内网IP'),(10786,'admin','获取调度日志信息',3,'com.febs.web.controller.quartz.JobLogController.index()','','127.0.0.1','2020-06-15 06:15:28','内网IP|0|0|内网IP|内网IP'),(10787,'admin','文章列表',3,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-15 06:15:30','内网IP|0|0|内网IP|内网IP'),(10788,'admin','获取文章列表信息',17,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@479d7fff  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@cc16801\"','127.0.0.1','2020-06-15 06:15:30','内网IP|0|0|内网IP|内网IP'),(10789,'admin','获取文章列表信息',17,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@60e6f66b  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@4ceb8b8f\"','127.0.0.1','2020-06-15 06:15:35','内网IP|0|0|内网IP|内网IP'),(10790,'admin','获取文章列表信息',6,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@5ae2f0b4  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@79d918a3\"','127.0.0.1','2020-06-15 06:15:35','内网IP|0|0|内网IP|内网IP'),(10791,'admin','获取文章列表信息',7,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@7f6e1dce  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@531a5769\"','127.0.0.1','2020-06-15 06:15:36','内网IP|0|0|内网IP|内网IP'),(10792,'admin','标签列表',3,'com.febs.web.controller.admin.cms.ArticleTagController.index()','','127.0.0.1','2020-06-15 06:15:40','内网IP|0|0|内网IP|内网IP'),(10793,'admin','获取标签列表信息',10,'com.febs.web.controller.admin.cms.ArticleTagController.articleList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleTag: \"com.febs.cms.domain.ArticleTag@2d77c605\"','127.0.0.1','2020-06-15 06:15:40','内网IP|0|0|内网IP|内网IP'),(10794,'admin','评论列表',2,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 06:15:42','内网IP|0|0|内网IP|内网IP'),(10795,'admin','获取文章评论列表信息',12,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@679b0985\"','127.0.0.1','2020-06-15 06:15:42','内网IP|0|0|内网IP|内网IP'),(10796,'admin','获取友情链接信息',3,'com.febs.web.controller.admin.cms.FriendLinkController.index()','','127.0.0.1','2020-06-15 06:15:43','内网IP|0|0|内网IP|内网IP'),(10797,'admin','获取分类信息',3,'com.febs.web.controller.admin.shop.ProductCategoryController.index()','','127.0.0.1','2020-06-15 06:15:45','内网IP|0|0|内网IP|内网IP'),(10798,'admin','订单列表',4,'com.febs.web.controller.admin.shop.OrderController.index()','','127.0.0.1','2020-06-15 06:15:50','内网IP|0|0|内网IP|内网IP'),(10799,'admin','获取订单列表信息',6,'com.febs.web.controller.admin.shop.OrderController.orderList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@71f1baaf  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  order: \"com.febs.shop.domain.Order@6872e937\"','127.0.0.1','2020-06-15 06:15:50','内网IP|0|0|内网IP|内网IP'),(10800,'admin','设置列表',3,'com.febs.web.controller.admin.cms.SettingController.index()','templates: \"[Ljava.lang.String;@10b99f41\"','127.0.0.1','2020-06-15 06:15:57','内网IP|0|0|内网IP|内网IP'),(10801,'admin','获取设置列表信息',6,'com.febs.web.controller.admin.cms.SettingController.settingList()','request: \"QueryRequest{pageSize=0, pageNum=0}\"  setting: \"com.febs.cms.domain.Setting@29fe0678\"','127.0.0.1','2020-06-15 06:15:57','内网IP|0|0|内网IP|内网IP'),(10802,'admin','获取导航信息',3,'com.febs.web.controller.admin.cms.NavMenuController.index()','','127.0.0.1','2020-06-15 06:16:17','内网IP|0|0|内网IP|内网IP'),(10803,'admin','设置列表',7,'com.febs.web.controller.admin.cms.ThemeController.index()','templates: \"[{id=layuiNetCompany, authorWebsite=http://www.febs.com, versionCode=1, updateUrl=, author=undead, version=1.0, screenshot=screenshot.png, title=layuiNetCompany 模板, description=这是一个学习模板}, {id=essence, authorWebsite=http://www.febs.com, versionCode=1, updateUrl=, author=wtsoftware, version=1.0, screenshot=screenshot.png, title=essence 模板, description=这是一个学习模板}]\"  theme: \"essence\"','127.0.0.1','2020-06-15 06:17:12','内网IP|0|0|内网IP|内网IP'),(10804,'admin','获取导航信息',0,'com.febs.web.controller.admin.cms.NavMenuController.index()','','127.0.0.1','2020-06-15 06:17:20','内网IP|0|0|内网IP|内网IP'),(10805,'admin','设置列表',0,'com.febs.web.controller.admin.cms.SettingController.index()','templates: \"[Ljava.lang.String;@fcb62e7\"','127.0.0.1','2020-06-15 06:17:22','内网IP|0|0|内网IP|内网IP'),(10806,'admin','获取设置列表信息',4,'com.febs.web.controller.admin.cms.SettingController.settingList()','request: \"QueryRequest{pageSize=0, pageNum=0}\"  setting: \"com.febs.cms.domain.Setting@56acf5a1\"','127.0.0.1','2020-06-15 06:17:22','内网IP|0|0|内网IP|内网IP'),(10807,'admin','设置列表',0,'com.febs.web.controller.admin.cms.SettingController.index()','templates: \"[Ljava.lang.String;@42941f92\"','127.0.0.1','2020-06-15 06:18:37','内网IP|0|0|内网IP|内网IP'),(10808,'admin','获取设置列表信息',5,'com.febs.web.controller.admin.cms.SettingController.settingList()','request: \"QueryRequest{pageSize=0, pageNum=0}\"  setting: \"com.febs.cms.domain.Setting@5385f4ff\"','127.0.0.1','2020-06-15 06:18:37','内网IP|0|0|内网IP|内网IP'),(10809,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:18:43','内网IP|0|0|内网IP|内网IP'),(10810,'admin','获取用户信息',5,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-15 06:18:48','内网IP|0|0|内网IP|内网IP'),(10811,'admin','获取角色信息',1,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 06:18:50','内网IP|0|0|内网IP|内网IP'),(10812,'admin','获取系统所有URL',2,'com.febs.web.controller.admin.system.MenuController.getAllUrl()','','127.0.0.1','2020-06-15 06:18:51','内网IP|0|0|内网IP|内网IP'),(10813,'admin','获取部门信息',1,'com.febs.web.controller.admin.system.DeptController.index()','','127.0.0.1','2020-06-15 06:18:52','内网IP|0|0|内网IP|内网IP'),(10814,'admin','获取用户信息',4,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-15 06:18:57','内网IP|0|0|内网IP|内网IP'),(10815,'admin','获取定时任务信息',0,'com.febs.web.controller.quartz.JobController.index()','','127.0.0.1','2020-06-15 06:19:03','内网IP|0|0|内网IP|内网IP'),(10816,'admin','获取友情链接信息',0,'com.febs.web.controller.admin.cms.FriendLinkController.index()','','127.0.0.1','2020-06-15 06:19:06','内网IP|0|0|内网IP|内网IP'),(10817,'admin','标签列表',0,'com.febs.web.controller.admin.cms.ArticleTagController.index()','','127.0.0.1','2020-06-15 06:19:06','内网IP|0|0|内网IP|内网IP'),(10818,'admin','获取标签列表信息',7,'com.febs.web.controller.admin.cms.ArticleTagController.articleList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleTag: \"com.febs.cms.domain.ArticleTag@26ccfaa6\"','127.0.0.1','2020-06-15 06:19:06','内网IP|0|0|内网IP|内网IP'),(10819,'admin','评论列表',0,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 06:19:07','内网IP|0|0|内网IP|内网IP'),(10820,'admin','获取文章评论列表信息',3,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@505d2728\"','127.0.0.1','2020-06-15 06:19:07','内网IP|0|0|内网IP|内网IP'),(10821,'admin','获取用户信息',6,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-15 06:22:21','内网IP|0|0|内网IP|内网IP'),(10822,'admin','获取角色信息',0,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 06:22:22','内网IP|0|0|内网IP|内网IP'),(10823,'admin','获取部门信息',0,'com.febs.web.controller.admin.system.DeptController.index()','','127.0.0.1','2020-06-15 06:22:24','内网IP|0|0|内网IP|内网IP'),(10824,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:22:34','内网IP|0|0|内网IP|内网IP'),(10825,'admin','获取系统所有URL',0,'com.febs.web.controller.admin.system.MenuController.getAllUrl()','','127.0.0.1','2020-06-15 06:22:38','内网IP|0|0|内网IP|内网IP'),(10826,'admin','获取部门信息',0,'com.febs.web.controller.admin.system.DeptController.index()','','127.0.0.1','2020-06-15 06:22:39','内网IP|0|0|内网IP|内网IP'),(10827,'admin','获取字典信息',0,'com.febs.web.controller.admin.system.DictController.index()','','127.0.0.1','2020-06-15 06:22:42','内网IP|0|0|内网IP|内网IP'),(10828,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:22:45','内网IP|0|0|内网IP|内网IP'),(10829,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:25:20','内网IP|0|0|内网IP|内网IP'),(10830,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:25:20','内网IP|0|0|内网IP|内网IP'),(10831,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:25:21','内网IP|0|0|内网IP|内网IP'),(10832,'admin','获取在线用户信息',1,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:25:21','内网IP|0|0|内网IP|内网IP'),(10833,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:25:21','内网IP|0|0|内网IP|内网IP'),(10834,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:27:18','内网IP|0|0|内网IP|内网IP'),(10835,'admin','获取定时任务信息',0,'com.febs.web.controller.quartz.JobController.index()','','127.0.0.1','2020-06-15 06:27:40','内网IP|0|0|内网IP|内网IP'),(10836,'admin','获取调度日志信息',0,'com.febs.web.controller.quartz.JobLogController.index()','','127.0.0.1','2020-06-15 06:27:41','内网IP|0|0|内网IP|内网IP'),(10837,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-15 06:27:43','内网IP|0|0|内网IP|内网IP'),(10838,'admin','获取文章列表信息',5,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@3509bb66  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@224e60d8\"','127.0.0.1','2020-06-15 06:27:43','内网IP|0|0|内网IP|内网IP'),(10839,'admin','获取分类信息',0,'com.febs.web.controller.admin.cms.ArticleCategoryController.index()','','127.0.0.1','2020-06-15 06:27:44','内网IP|0|0|内网IP|内网IP'),(10840,'admin','单页列表',3,'com.febs.web.controller.admin.cms.SinglePageController.index()','','127.0.0.1','2020-06-15 06:27:44','内网IP|0|0|内网IP|内网IP'),(10841,'admin','获取单页列表信息',13,'com.febs.web.controller.admin.cms.SinglePageController.singlePageList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  singlePage: \"com.febs.cms.domain.SinglePage@28a35ca0\"','127.0.0.1','2020-06-15 06:27:44','内网IP|0|0|内网IP|内网IP'),(10842,'admin','标签列表',0,'com.febs.web.controller.admin.cms.ArticleTagController.index()','','127.0.0.1','2020-06-15 06:27:46','内网IP|0|0|内网IP|内网IP'),(10843,'admin','获取标签列表信息',6,'com.febs.web.controller.admin.cms.ArticleTagController.articleList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleTag: \"com.febs.cms.domain.ArticleTag@580ae5c\"','127.0.0.1','2020-06-15 06:27:46','内网IP|0|0|内网IP|内网IP'),(10844,'admin','标签列表',0,'com.febs.web.controller.admin.cms.ArticleTagController.index()','','127.0.0.1','2020-06-15 06:27:46','内网IP|0|0|内网IP|内网IP'),(10845,'admin','获取标签列表信息',4,'com.febs.web.controller.admin.cms.ArticleTagController.articleList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleTag: \"com.febs.cms.domain.ArticleTag@2e3dc73e\"','127.0.0.1','2020-06-15 06:27:46','内网IP|0|0|内网IP|内网IP'),(10846,'admin','订单列表',0,'com.febs.web.controller.admin.shop.OrderController.index()','','127.0.0.1','2020-06-15 06:27:50','内网IP|0|0|内网IP|内网IP'),(10847,'admin','获取订单列表信息',6,'com.febs.web.controller.admin.shop.OrderController.orderList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@71dada4a  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  order: \"com.febs.shop.domain.Order@5f8eb7bb\"','127.0.0.1','2020-06-15 06:27:50','内网IP|0|0|内网IP|内网IP'),(10848,'admin','设置列表',0,'com.febs.web.controller.admin.cms.SettingController.index()','templates: \"[Ljava.lang.String;@2ae26b63\"','127.0.0.1','2020-06-15 06:27:53','内网IP|0|0|内网IP|内网IP'),(10849,'admin','获取设置列表信息',3,'com.febs.web.controller.admin.cms.SettingController.settingList()','request: \"QueryRequest{pageSize=0, pageNum=0}\"  setting: \"com.febs.cms.domain.Setting@5432ea8e\"','127.0.0.1','2020-06-15 06:27:53','内网IP|0|0|内网IP|内网IP'),(10850,'admin','获取导航信息',0,'com.febs.web.controller.admin.cms.NavMenuController.index()','','127.0.0.1','2020-06-15 06:27:54','内网IP|0|0|内网IP|内网IP'),(10851,'admin','设置列表',2,'com.febs.web.controller.admin.cms.ThemeController.index()','templates: \"[{id=layuiNetCompany, authorWebsite=http://www.febs.com, versionCode=1, updateUrl=, author=undead, version=1.0, screenshot=screenshot.png, title=layuiNetCompany 模板, description=这是一个学习模板}, {id=essence, authorWebsite=http://www.febs.com, versionCode=1, updateUrl=, author=wtsoftware, version=1.0, screenshot=screenshot.png, title=essence 模板, description=这是一个学习模板}]\"  theme: \"essence\"','127.0.0.1','2020-06-15 06:27:55','内网IP|0|0|内网IP|内网IP'),(10852,'admin','获取用户信息',5,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-15 06:27:59','内网IP|0|0|内网IP|内网IP'),(10853,'admin','获取角色信息',1,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 06:28:00','内网IP|0|0|内网IP|内网IP'),(10854,'admin','获取系统所有URL',1,'com.febs.web.controller.admin.system.MenuController.getAllUrl()','','127.0.0.1','2020-06-15 06:28:01','内网IP|0|0|内网IP|内网IP'),(10855,'admin','获取部门信息',0,'com.febs.web.controller.admin.system.DeptController.index()','','127.0.0.1','2020-06-15 06:28:02','内网IP|0|0|内网IP|内网IP'),(10856,'admin','获取字典信息',0,'com.febs.web.controller.admin.system.DictController.index()','','127.0.0.1','2020-06-15 06:28:03','内网IP|0|0|内网IP|内网IP'),(10857,'admin','获取在线用户信息',0,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-15 06:28:05','内网IP|0|0|内网IP|内网IP'),(10858,'admin','获取定时任务信息',0,'com.febs.web.controller.quartz.JobController.index()','','127.0.0.1','2020-06-15 06:28:09','内网IP|0|0|内网IP|内网IP'),(10859,'admin','获取调度日志信息',0,'com.febs.web.controller.quartz.JobLogController.index()','','127.0.0.1','2020-06-15 06:28:09','内网IP|0|0|内网IP|内网IP'),(10860,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-15 06:28:11','内网IP|0|0|内网IP|内网IP'),(10861,'admin','获取文章列表信息',5,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@7c11a2eb  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@566f848f\"','127.0.0.1','2020-06-15 06:28:11','内网IP|0|0|内网IP|内网IP'),(10862,'admin','获取分类信息',0,'com.febs.web.controller.admin.cms.ArticleCategoryController.index()','','127.0.0.1','2020-06-15 06:28:12','内网IP|0|0|内网IP|内网IP'),(10863,'admin','单页列表',0,'com.febs.web.controller.admin.cms.SinglePageController.index()','','127.0.0.1','2020-06-15 06:28:12','内网IP|0|0|内网IP|内网IP'),(10864,'admin','获取单页列表信息',4,'com.febs.web.controller.admin.cms.SinglePageController.singlePageList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  singlePage: \"com.febs.cms.domain.SinglePage@4ab7ccb7\"','127.0.0.1','2020-06-15 06:28:13','内网IP|0|0|内网IP|内网IP'),(10865,'admin','获取友情链接信息',0,'com.febs.web.controller.admin.cms.FriendLinkController.index()','','127.0.0.1','2020-06-15 06:28:15','内网IP|0|0|内网IP|内网IP'),(10866,'admin','标签列表',0,'com.febs.web.controller.admin.cms.ArticleTagController.index()','','127.0.0.1','2020-06-15 06:28:16','内网IP|0|0|内网IP|内网IP'),(10867,'admin','获取标签列表信息',3,'com.febs.web.controller.admin.cms.ArticleTagController.articleList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleTag: \"com.febs.cms.domain.ArticleTag@551cc102\"','127.0.0.1','2020-06-15 06:28:16','内网IP|0|0|内网IP|内网IP'),(10868,'admin','评论列表',0,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 06:28:17','内网IP|0|0|内网IP|内网IP'),(10869,'admin','获取文章评论列表信息',4,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@6708c8e\"','127.0.0.1','2020-06-15 06:28:17','内网IP|0|0|内网IP|内网IP'),(10870,'admin','评论列表',0,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 06:33:05','内网IP|0|0|内网IP|内网IP'),(10871,'admin','获取文章评论列表信息',2,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@6f656357\"','127.0.0.1','2020-06-15 06:33:06','内网IP|0|0|内网IP|内网IP'),(10872,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-15 06:33:08','内网IP|0|0|内网IP|内网IP'),(10873,'admin','获取文章列表信息',4,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@6f9e0ac6  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@6df441ad\"','127.0.0.1','2020-06-15 06:33:08','内网IP|0|0|内网IP|内网IP'),(10874,'admin','获取分类信息',0,'com.febs.web.controller.admin.cms.ArticleCategoryController.index()','','127.0.0.1','2020-06-15 06:33:09','内网IP|0|0|内网IP|内网IP'),(10875,'admin','单页列表',0,'com.febs.web.controller.admin.cms.SinglePageController.index()','','127.0.0.1','2020-06-15 06:46:57','内网IP|0|0|内网IP|内网IP'),(10876,'admin','获取单页列表信息',4,'com.febs.web.controller.admin.cms.SinglePageController.singlePageList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  singlePage: \"com.febs.cms.domain.SinglePage@15ebb1e6\"','127.0.0.1','2020-06-15 06:46:58','内网IP|0|0|内网IP|内网IP'),(10877,'admin','评论列表',4,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 10:36:12','内网IP|0|0|内网IP|内网IP'),(10878,'admin','获取文章评论列表信息',12,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@217400eb\"','127.0.0.1','2020-06-15 10:36:12','内网IP|0|0|内网IP|内网IP'),(10879,'admin','评论列表',0,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 10:36:31','内网IP|0|0|内网IP|内网IP'),(10880,'admin','获取文章评论列表信息',6,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@30c10950\"','127.0.0.1','2020-06-15 10:36:31','内网IP|0|0|内网IP|内网IP'),(10881,'admin','评论列表',0,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-15 10:36:48','内网IP|0|0|内网IP|内网IP'),(10882,'admin','获取文章评论列表信息',5,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@7cbf7f7d\"','127.0.0.1','2020-06-15 10:36:48','内网IP|0|0|内网IP|内网IP'),(10883,'admin','获取系统所有URL',12,'com.febs.web.controller.admin.system.MenuController.getAllUrl()','','127.0.0.1','2020-06-15 10:58:41','内网IP|0|0|内网IP|内网IP'),(10884,'admin','获取角色信息',3,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 11:04:35','内网IP|0|0|内网IP|内网IP'),(10885,'admin','修改角色',98,'com.febs.web.controller.admin.system.RoleController.updateRole()','role: \"com.febs.system.domain.Role@148d4f9b\"  menuId: \"[Ljava.lang.Long;@76921848\"','127.0.0.1','2020-06-15 11:04:44','内网IP|0|0|内网IP|内网IP'),(10886,'admin','获取角色信息',0,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 11:04:47','内网IP|0|0|内网IP|内网IP'),(10887,'admin','获取角色信息',1,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-15 11:05:03','内网IP|0|0|内网IP|内网IP'),(10888,'admin','评论列表',4,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-15 11:05:30','内网IP|0|0|内网IP|内网IP'),(10889,'admin','获取商品评论列表信息',7,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@4dce25c7\"','127.0.0.1','2020-06-15 11:05:30','内网IP|0|0|内网IP|内网IP'),(10890,'admin','评论列表',0,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-15 11:09:14','内网IP|0|0|内网IP|内网IP'),(10891,'admin','获取商品评论列表信息',6,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@47614226\"','127.0.0.1','2020-06-15 11:09:14','内网IP|0|0|内网IP|内网IP'),(10892,'admin','评论列表',0,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-15 11:09:49','内网IP|0|0|内网IP|内网IP'),(10893,'admin','获取商品评论列表信息',6,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@754092ab\"','127.0.0.1','2020-06-15 11:09:49','内网IP|0|0|内网IP|内网IP'),(10894,'admin','评论列表',0,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-15 11:10:38','内网IP|0|0|内网IP|内网IP'),(10895,'admin','获取商品评论列表信息',7,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@718e63c8\"','127.0.0.1','2020-06-15 11:10:38','内网IP|0|0|内网IP|内网IP'),(10896,'admin','评论列表',0,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-15 11:12:54','内网IP|0|0|内网IP|内网IP'),(10897,'admin','获取商品评论列表信息',4,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@14f176ec\"','127.0.0.1','2020-06-15 11:12:55','内网IP|0|0|内网IP|内网IP'),(10898,'admin','更新商品评论',5,'com.febs.web.controller.admin.shop.ProductCommentController.updateProductComment()','productComment: \"com.febs.shop.domain.ProductComment@4d932b74\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@3ea907f7','127.0.0.1','2020-06-15 11:15:10','内网IP|0|0|内网IP|内网IP'),(10899,'admin','获取商品评论列表信息',3,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@38f37309\"','127.0.0.1','2020-06-15 11:15:10','内网IP|0|0|内网IP|内网IP'),(10900,'admin','评论列表',3,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-15 15:19:35','内网IP|0|0|内网IP|内网IP'),(10901,'admin','获取商品评论列表信息',6,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@3635836c\"','127.0.0.1','2020-06-15 15:19:35','内网IP|0|0|内网IP|内网IP'),(10902,'admin','更新商品评论',5,'com.febs.web.controller.admin.shop.ProductCommentController.updateProductComment()','productComment: \"com.febs.shop.domain.ProductComment@38ca76db\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@bae5fd','127.0.0.1','2020-06-15 15:19:43','内网IP|0|0|内网IP|内网IP'),(10903,'admin','获取商品评论列表信息',4,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@680735ff\"','127.0.0.1','2020-06-15 15:19:43','内网IP|0|0|内网IP|内网IP'),(10904,'admin','更新商品评论',5,'com.febs.web.controller.admin.shop.ProductCommentController.updateProductComment()','productComment: \"com.febs.shop.domain.ProductComment@310d23af\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@235ef179','127.0.0.1','2020-06-15 15:19:49','内网IP|0|0|内网IP|内网IP'),(10905,'admin','获取商品评论列表信息',3,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@6308bc57\"','127.0.0.1','2020-06-15 15:19:49','内网IP|0|0|内网IP|内网IP'),(10906,'admin','文章列表',3,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-16 01:23:07','内网IP|0|0|内网IP|内网IP'),(10907,'admin','获取文章列表信息',9,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@1fa7077b  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@71e62f5c\"','127.0.0.1','2020-06-16 01:23:08','内网IP|0|0|内网IP|内网IP'),(10908,'admin','获取分类信息',1,'com.febs.web.controller.admin.cms.ArticleCategoryController.index()','','127.0.0.1','2020-06-16 01:23:09','内网IP|0|0|内网IP|内网IP'),(10909,'admin','单页列表',3,'com.febs.web.controller.admin.cms.SinglePageController.index()','','127.0.0.1','2020-06-16 01:23:11','内网IP|0|0|内网IP|内网IP'),(10910,'admin','获取单页列表信息',7,'com.febs.web.controller.admin.cms.SinglePageController.singlePageList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  singlePage: \"com.febs.cms.domain.SinglePage@3b528536\"','127.0.0.1','2020-06-16 01:23:11','内网IP|0|0|内网IP|内网IP'),(10911,'admin','获取友情链接信息',3,'com.febs.web.controller.admin.cms.FriendLinkController.index()','','127.0.0.1','2020-06-16 01:23:16','内网IP|0|0|内网IP|内网IP'),(10912,'admin','标签列表',3,'com.febs.web.controller.admin.cms.ArticleTagController.index()','','127.0.0.1','2020-06-16 01:23:18','内网IP|0|0|内网IP|内网IP'),(10913,'admin','获取标签列表信息',11,'com.febs.web.controller.admin.cms.ArticleTagController.articleList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleTag: \"com.febs.cms.domain.ArticleTag@2bea3a6f\"','127.0.0.1','2020-06-16 01:23:18','内网IP|0|0|内网IP|内网IP'),(10914,'admin','评论列表',2,'com.febs.web.controller.admin.cms.ArticleCommentController.index()','','127.0.0.1','2020-06-16 01:23:21','内网IP|0|0|内网IP|内网IP'),(10915,'admin','获取文章评论列表信息',14,'com.febs.web.controller.admin.cms.ArticleCommentController.articleCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  articleComment: \"com.febs.cms.domain.ArticleComment@518cd383\"','127.0.0.1','2020-06-16 01:23:21','内网IP|0|0|内网IP|内网IP'),(10916,'admin','评论列表',3,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-16 01:23:24','内网IP|0|0|内网IP|内网IP'),(10917,'admin','获取商品评论列表信息',11,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@70f6932c\"','127.0.0.1','2020-06-16 01:23:24','内网IP|0|0|内网IP|内网IP'),(10918,'admin','评论列表',0,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-16 01:33:57','内网IP|0|0|内网IP|内网IP'),(10919,'admin','获取商品评论列表信息',5,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@1b039fc9\"','127.0.0.1','2020-06-16 01:33:57','内网IP|0|0|内网IP|内网IP'),(10920,'admin','更新商品评论',5,'com.febs.web.controller.admin.shop.ProductCommentController.updateProductComment()','productComment: \"com.febs.shop.domain.ProductComment@6493beba\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@31d32469','127.0.0.1','2020-06-16 01:34:04','内网IP|0|0|内网IP|内网IP'),(10921,'admin','获取商品评论列表信息',3,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@261ddb26\"','127.0.0.1','2020-06-16 01:34:04','内网IP|0|0|内网IP|内网IP'),(10922,'admin','订单列表',5,'com.febs.web.controller.admin.shop.OrderController.index()','','127.0.0.1','2020-06-16 01:56:04','内网IP|0|0|内网IP|内网IP'),(10923,'admin','获取订单列表信息',10,'com.febs.web.controller.admin.shop.OrderController.orderList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@2dfed2a1  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  order: \"com.febs.shop.domain.Order@18df120c\"','127.0.0.1','2020-06-16 01:56:04','内网IP|0|0|内网IP|内网IP'),(10924,'admin','评论列表',3,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-16 01:56:05','内网IP|0|0|内网IP|内网IP'),(10925,'admin','获取商品评论列表信息',12,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@19003394\"','127.0.0.1','2020-06-16 01:56:05','内网IP|0|0|内网IP|内网IP'),(10926,'admin','评论列表',0,'com.febs.web.controller.admin.shop.ProductCommentController.index()','','127.0.0.1','2020-06-16 01:57:04','内网IP|0|0|内网IP|内网IP'),(10927,'admin','获取商品评论列表信息',8,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@7c4a0b75\"','127.0.0.1','2020-06-16 01:57:04','内网IP|0|0|内网IP|内网IP'),(10928,'admin','更新商品评论',5,'com.febs.web.controller.admin.shop.ProductCommentController.updateProductComment()','productComment: \"com.febs.shop.domain.ProductComment@1939c9e\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@549a9409','127.0.0.1','2020-06-16 01:57:15','内网IP|0|0|内网IP|内网IP'),(10929,'admin','获取商品评论列表信息',4,'com.febs.web.controller.admin.shop.ProductCommentController.productCommentList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  productComment: \"com.febs.shop.domain.ProductComment@1609aed9\"','127.0.0.1','2020-06-16 01:57:15','内网IP|0|0|内网IP|内网IP'),(10930,'admin','订单列表',4,'com.febs.web.controller.admin.shop.OrderController.index()','','127.0.0.1','2020-06-16 07:21:12','内网IP|0|0|内网IP|内网IP'),(10931,'admin','获取订单列表信息',4,'com.febs.web.controller.admin.shop.OrderController.orderList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@321e61c7  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  order: \"com.febs.shop.domain.Order@4a9aa4e7\"','127.0.0.1','2020-06-16 07:21:12','内网IP|0|0|内网IP|内网IP'),(10932,'admin','获取用户信息',11,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-16 07:21:43','内网IP|0|0|内网IP|内网IP'),(10933,'admin','获取定时任务信息',3,'com.febs.web.controller.quartz.JobController.index()','','127.0.0.1','2020-06-16 07:26:00','内网IP|0|0|内网IP|内网IP'),(10934,'admin','获取在线用户信息',2,'com.febs.web.controller.security.SessionController.online()','','127.0.0.1','2020-06-16 07:26:02','内网IP|0|0|内网IP|内网IP'),(10935,'admin','获取用户信息',4,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-16 07:26:06','内网IP|0|0|内网IP|内网IP'),(10936,'admin','获取角色信息',0,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-16 07:26:06','内网IP|0|0|内网IP|内网IP'),(10937,'admin','获取系统所有URL',11,'com.febs.web.controller.admin.system.MenuController.getAllUrl()','','127.0.0.1','2020-06-16 07:26:07','内网IP|0|0|内网IP|内网IP'),(10938,'admin','获取字典信息',3,'com.febs.web.controller.admin.system.DictController.index()','','127.0.0.1','2020-06-16 07:26:08','内网IP|0|0|内网IP|内网IP'),(10939,'admin','设置列表',6,'com.febs.web.controller.admin.cms.ThemeController.index()','templates: \"[{id=layuiNetCompany, authorWebsite=http://www.febs.com, versionCode=1, updateUrl=, author=undead, version=1.0, screenshot=screenshot.png, title=layuiNetCompany 模板, description=这是一个学习模板}, {id=essence, authorWebsite=http://www.febs.com, versionCode=1, updateUrl=, author=wtsoftware, version=1.0, screenshot=screenshot.png, title=essence 模板, description=这是一个学习模板}]\"  theme: \"essence\"','127.0.0.1','2020-06-16 07:26:14','内网IP|0|0|内网IP|内网IP'),(10940,'admin','获取导航信息',2,'com.febs.web.controller.admin.cms.NavMenuController.index()','','127.0.0.1','2020-06-16 07:26:15','内网IP|0|0|内网IP|内网IP'),(10941,'admin','设置列表',3,'com.febs.web.controller.admin.cms.SettingController.index()','templates: \"[Ljava.lang.String;@13d5d760\"','127.0.0.1','2020-06-16 07:26:15','内网IP|0|0|内网IP|内网IP'),(10942,'admin','获取设置列表信息',5,'com.febs.web.controller.admin.cms.SettingController.settingList()','request: \"QueryRequest{pageSize=0, pageNum=0}\"  setting: \"com.febs.cms.domain.Setting@14ab5637\"','127.0.0.1','2020-06-16 07:26:15','内网IP|0|0|内网IP|内网IP'),(10943,'admin','文章列表',5,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-18 09:22:29','内网IP|0|0|内网IP|内网IP'),(10944,'admin','获取文章列表信息',19,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@53c7b25b  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@39e5c3d1\"','127.0.0.1','2020-06-18 09:22:29','内网IP|0|0|内网IP|内网IP'),(10945,'admin','获取分类信息',1,'com.febs.web.controller.admin.cms.ArticleCategoryController.index()','','127.0.0.1','2020-06-18 09:22:30','内网IP|0|0|内网IP|内网IP'),(10946,'admin','单页列表',4,'com.febs.web.controller.admin.cms.SinglePageController.index()','','127.0.0.1','2020-06-18 09:22:31','内网IP|0|0|内网IP|内网IP'),(10947,'admin','获取单页列表信息',9,'com.febs.web.controller.admin.cms.SinglePageController.singlePageList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  singlePage: \"com.febs.cms.domain.SinglePage@735bf56b\"','127.0.0.1','2020-06-18 09:22:32','内网IP|0|0|内网IP|内网IP'),(10948,'admin','获取友情链接信息',4,'com.febs.web.controller.admin.cms.FriendLinkController.index()','','127.0.0.1','2020-06-18 09:22:33','内网IP|0|0|内网IP|内网IP'),(10949,'admin','获取用户信息',11,'com.febs.web.controller.admin.system.UserController.userList()','request: \"QueryRequest{pageSize=10, pageNum=1}\"  user: \"MyUser{userId=null, username=\'\', password=\'null\', deptId=null, deptName=\'null\', email=\'null\', mobile=\'null\', status=\'null\', createTime=null, modifyTime=null, lastLoginTime=null, ssex=\'null\', theme=\'null\', avatar=\'null\', description=\'null\', roleName=\'null\'}\"','127.0.0.1','2020-06-18 09:25:01','内网IP|0|0|内网IP|内网IP'),(10950,'admin','获取角色信息',0,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-18 09:25:02','内网IP|0|0|内网IP|内网IP'),(10951,'admin','获取角色信息',0,'com.febs.web.controller.admin.system.RoleController.index()','','127.0.0.1','2020-06-18 09:25:13','内网IP|0|0|内网IP|内网IP'),(10952,'admin','文章列表',7,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 00:35:51','内网IP|0|0|内网IP|内网IP'),(10953,'admin','获取文章列表信息',35,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@6efdeb5c  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@15d2a38b\"','127.0.0.1','2020-06-19 00:35:51','内网IP|0|0|内网IP|内网IP'),(10954,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 00:36:28','内网IP|0|0|内网IP|内网IP'),(10955,'admin','获取文章列表信息',14,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@cae4cfe  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@5a25c0ac\"','127.0.0.1','2020-06-19 00:36:28','内网IP|0|0|内网IP|内网IP'),(10956,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 00:37:13','内网IP|0|0|内网IP|内网IP'),(10957,'admin','获取文章列表信息',9,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@4af2a95  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@50131edd\"','127.0.0.1','2020-06-19 00:37:14','内网IP|0|0|内网IP|内网IP'),(10958,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 00:37:34','内网IP|0|0|内网IP|内网IP'),(10959,'admin','获取文章列表信息',9,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@5e81b25d  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@3e816fb\"','127.0.0.1','2020-06-19 00:37:35','内网IP|0|0|内网IP|内网IP'),(10960,'admin','文章列表',0,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 00:38:27','内网IP|0|0|内网IP|内网IP'),(10961,'admin','获取文章列表信息',8,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@65bd817e  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@5878bde3\"','127.0.0.1','2020-06-19 00:38:27','内网IP|0|0|内网IP|内网IP'),(10962,'admin','修改文章',55,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@2678bce\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@6d741304','127.0.0.1','2020-06-19 00:39:03','内网IP|0|0|内网IP|内网IP'),(10963,'admin','获取文章列表信息',7,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@77a7b387  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@3db54348\"','127.0.0.1','2020-06-19 00:39:03','内网IP|0|0|内网IP|内网IP'),(10964,'admin','修改文章',76,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@57d7a8a4\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@3c84d973','127.0.0.1','2020-06-19 00:39:21','内网IP|0|0|内网IP|内网IP'),(10965,'admin','修改文章',20,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@36910e8\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@173aa446','127.0.0.1','2020-06-19 00:39:51','内网IP|0|0|内网IP|内网IP'),(10966,'admin','获取文章列表信息',16,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@60dd1b88  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@6dccdd03\"','127.0.0.1','2020-06-19 00:39:51','内网IP|0|0|内网IP|内网IP'),(10967,'admin','文章列表',5,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 00:54:23','内网IP|0|0|内网IP|内网IP'),(10968,'admin','获取文章列表信息',17,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@17225110  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@4d58eb92\"','127.0.0.1','2020-06-19 00:54:24','内网IP|0|0|内网IP|内网IP'),(10969,'admin','修改文章',52,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@1a8c4ecd\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@4f357200','127.0.0.1','2020-06-19 00:54:36','内网IP|0|0|内网IP|内网IP'),(10970,'admin','获取文章列表信息',42,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@6bd92dc4  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@5013e80e\"','127.0.0.1','2020-06-19 00:54:36','内网IP|0|0|内网IP|内网IP'),(10971,'admin','文章列表',5,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 01:08:27','内网IP|0|0|内网IP|内网IP'),(10972,'admin','获取文章列表信息',18,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@7f03bb3b  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@3393d6c8\"','127.0.0.1','2020-06-19 01:08:27','内网IP|0|0|内网IP|内网IP'),(10973,'admin','修改文章',44,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@7aa14017\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@4b5feb3b','127.0.0.1','2020-06-19 01:08:34','内网IP|0|0|内网IP|内网IP'),(10974,'admin','获取文章列表信息',28,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@a84a95d  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@5c157977\"','127.0.0.1','2020-06-19 01:08:34','内网IP|0|0|内网IP|内网IP'),(10975,'admin','文章列表',7,'com.febs.web.controller.admin.cms.ArticleController.index()','','127.0.0.1','2020-06-19 01:13:01','内网IP|0|0|内网IP|内网IP'),(10976,'admin','获取文章列表信息',21,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@17bbec6  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@772a6d8f\"','127.0.0.1','2020-06-19 01:13:01','内网IP|0|0|内网IP|内网IP'),(10977,'admin','修改文章',47,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@696569ca\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@7416d542','127.0.0.1','2020-06-19 01:13:08','内网IP|0|0|内网IP|内网IP'),(10978,'admin','获取文章列表信息',15,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@1a381e2a  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@3cef16f3\"','127.0.0.1','2020-06-19 01:13:08','内网IP|0|0|内网IP|内网IP'),(10979,'admin','修改文章',30,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@2a5c236c\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@4a9f82cf','127.0.0.1','2020-06-19 01:13:16','内网IP|0|0|内网IP|内网IP'),(10980,'admin','获取文章列表信息',32,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@2caf2cf0  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@1497ae41\"','127.0.0.1','2020-06-19 01:13:16','内网IP|0|0|内网IP|内网IP'),(10981,'admin','修改文章',5,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@5a3cb0b8\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@60364932','127.0.0.1','2020-06-19 01:13:22','内网IP|0|0|内网IP|内网IP'),(10982,'admin','修改文章',9,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@6b0986ef\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@754c16d3','127.0.0.1','2020-06-19 01:14:37','内网IP|0|0|内网IP|内网IP'),(10983,'admin','修改文章',4,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@65b1460f\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@1ffbcd46','127.0.0.1','2020-06-19 01:15:19','内网IP|0|0|内网IP|内网IP'),(10984,'admin','修改文章',2,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@1a39281a\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@49366bd4','127.0.0.1','2020-06-19 01:16:03','内网IP|0|0|内网IP|内网IP'),(10985,'admin','修改文章',6946,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@1f8cde83\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@7851ec4','127.0.0.1','2020-06-19 01:16:36','内网IP|0|0|内网IP|内网IP'),(10986,'admin','获取文章列表信息',26,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@3570ee42  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@4383d8c3\"','127.0.0.1','2020-06-19 01:16:36','内网IP|0|0|内网IP|内网IP'),(10987,'admin','修改文章',20,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@36618eae\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@71960602','127.0.0.1','2020-06-19 01:16:47','内网IP|0|0|内网IP|内网IP'),(10988,'admin','获取文章列表信息',12,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@736138d8  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@68f036e2\"','127.0.0.1','2020-06-19 01:16:47','内网IP|0|0|内网IP|内网IP'),(10989,'admin','修改文章',5,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@57d599f9\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@4829f1f5','127.0.0.1','2020-06-19 01:16:54','内网IP|0|0|内网IP|内网IP'),(10990,'admin','修改文章',19,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@a8ffe2f\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@37f6729d','127.0.0.1','2020-06-19 01:16:59','内网IP|0|0|内网IP|内网IP'),(10991,'admin','获取文章列表信息',8,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@4369984  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@6c081809\"','127.0.0.1','2020-06-19 01:16:59','内网IP|0|0|内网IP|内网IP'),(10992,'admin','修改文章',21,'com.febs.web.controller.admin.cms.ArticleController.updateArticle()','article: \"com.febs.cms.domain.Article@39b2a2fa\"  request: com.febs.security.xss.XssHttpServletRequestWrapper@6e57e18a','127.0.0.1','2020-06-19 01:17:05','内网IP|0|0|内网IP|内网IP'),(10993,'admin','获取文章列表信息',16,'com.febs.web.controller.admin.cms.ArticleController.articleList()','request: com.febs.security.xss.XssHttpServletRequestWrapper@1acb2459  queryRequest: \"QueryRequest{pageSize=10, pageNum=1}\"  article: \"com.febs.cms.domain.Article@213b2cb7\"','127.0.0.1','2020-06-19 01:17:05','内网IP|0|0|内网IP|内网IP');
/*!40000 ALTER TABLE `t_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_menu`
--

DROP TABLE IF EXISTS `t_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_menu` (
  `MENU_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单/按钮ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级菜单ID',
  `MENU_NAME` varchar(50) NOT NULL COMMENT '菜单/按钮名称',
  `URL` varchar(100) DEFAULT NULL COMMENT '菜单URL',
  `PERMS` text COMMENT '权限标识',
  `ICON` varchar(50) DEFAULT NULL COMMENT '图标',
  `TYPE` char(2) NOT NULL COMMENT '类型 0菜单 1按钮',
  `ORDER_NUM` bigint(20) DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`MENU_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_menu`
--

LOCK TABLES `t_menu` WRITE;
/*!40000 ALTER TABLE `t_menu` DISABLE KEYS */;
INSERT INTO `t_menu` VALUES (1,0,'系统管理','','','mdi mdi-airplay','0',1,'2017-12-27 16:39:07','2020-03-28 11:39:49'),(2,0,'系统监控','','','mdi mdi-cloud-sync','0',2,'2017-12-27 16:45:51','2020-03-28 11:35:05'),(3,1,'用户管理','admin/user','user:list','','0',1,'2017-12-27 16:47:13','2018-04-25 09:00:01'),(4,1,'角色管理','admin/role','role:list','mdi mdi-account-plus','0',2,'2017-12-27 16:48:09','2020-03-28 11:30:08'),(5,1,'菜单管理','admin/menu','menu:list','','0',3,'2017-12-27 16:48:57','2018-04-25 09:01:30'),(6,1,'部门管理','admin/dept','dept:list','','0',4,'2017-12-27 16:57:33','2018-04-25 09:01:40'),(8,2,'在线用户','session','session:list','','0',1,'2017-12-27 16:59:33','2018-04-25 09:02:04'),(10,2,'系统日志','admin/log','log:list','','0',3,'2017-12-27 17:00:50','2018-04-25 09:02:18'),(11,3,'新增用户','','user:add','','1',NULL,'2017-12-27 17:02:58','2020-02-01 17:05:48'),(12,3,'修改用户',NULL,'user:update',NULL,'1',NULL,'2017-12-27 17:04:07',NULL),(13,3,'删除用户',NULL,'user:delete',NULL,'1',NULL,'2017-12-27 17:04:58',NULL),(14,4,'新增角色',NULL,'role:add',NULL,'1',NULL,'2017-12-27 17:06:38',NULL),(15,4,'修改角色',NULL,'role:update',NULL,'1',NULL,'2017-12-27 17:06:38',NULL),(16,4,'删除角色',NULL,'role:delete',NULL,'1',NULL,'2017-12-27 17:06:38',NULL),(17,5,'新增菜单',NULL,'menu:add',NULL,'1',NULL,'2017-12-27 17:08:02',NULL),(18,5,'修改菜单',NULL,'menu:update',NULL,'1',NULL,'2017-12-27 17:08:02',NULL),(19,5,'删除菜单',NULL,'menu:delete',NULL,'1',NULL,'2017-12-27 17:08:02',NULL),(20,6,'新增部门',NULL,'dept:add',NULL,'1',NULL,'2017-12-27 17:09:24',NULL),(21,6,'修改部门',NULL,'dept:update',NULL,'1',NULL,'2017-12-27 17:09:24',NULL),(22,6,'删除部门',NULL,'dept:delete',NULL,'1',NULL,'2017-12-27 17:09:24',NULL),(23,8,'踢出用户','','session:kickout','','1',NULL,'2017-12-27 17:11:13','2018-09-06 18:45:27'),(24,10,'删除日志',NULL,'log:delete',NULL,'1',NULL,'2017-12-27 17:11:45',NULL),(64,1,'字典管理','admin/dict','dict:list','','0',NULL,'2018-01-18 10:38:25','2018-09-07 21:23:58'),(65,64,'新增字典',NULL,'dict:add',NULL,'1',NULL,'2018-01-18 19:10:08',NULL),(66,64,'修改字典',NULL,'dict:update',NULL,'1',NULL,'2018-01-18 19:10:27',NULL),(67,64,'删除字典',NULL,'dict:delete',NULL,'1',NULL,'2018-01-18 19:10:47',NULL),(101,0,'任务调度','','','mdi mdi-alarm-check','0',NULL,'2018-02-24 15:52:57','2020-03-28 11:13:56'),(102,101,'定时任务','admin/job','job:list','','0',NULL,'2018-02-24 15:53:53','2020-05-10 07:55:38'),(103,102,'新增任务',NULL,'job:add',NULL,'1',NULL,'2018-02-24 15:55:10',NULL),(104,102,'修改任务',NULL,'job:update',NULL,'1',NULL,'2018-02-24 15:55:53',NULL),(105,102,'删除任务',NULL,'job:delete',NULL,'1',NULL,'2018-02-24 15:56:18',NULL),(106,102,'暂停任务',NULL,'job:pause',NULL,'1',NULL,'2018-02-24 15:57:08',NULL),(107,102,'恢复任务',NULL,'job:resume',NULL,'1',NULL,'2018-02-24 15:58:21',NULL),(108,102,'立即执行任务',NULL,'job:run',NULL,'1',NULL,'2018-02-24 15:59:45',NULL),(109,101,'调度日志','admin/jobLog','jobLog:list','','0',NULL,'2018-02-24 16:00:45','2020-05-13 00:32:17'),(110,109,'删除日志',NULL,'jobLog:delete',NULL,'1',NULL,'2018-02-24 16:01:21',NULL),(113,2,'Redis监控','redis/info','redis:list','','0',NULL,'2018-06-28 14:29:42',NULL),(114,2,'Redis终端','redis/terminal','redis:terminal','','0',NULL,'2018-06-28 15:35:21',NULL),(117,135,'文章管理','admin/article','article:list','','0',NULL,'2020-02-03 12:02:13','2020-02-10 08:43:20'),(118,135,'分类管理','admin/category','category:list','','0',NULL,'2020-02-04 06:39:51','2020-02-10 08:43:50'),(119,118,'新增分类','','category:add','','1',NULL,'2020-02-04 06:40:54','2020-02-04 06:42:25'),(120,118,'修改分类','','category:update','','1',NULL,'2020-02-04 06:42:48',NULL),(121,118,'删除分类','','category:delete','','1',NULL,'2020-02-04 06:43:09',NULL),(122,117,'新增文章','','article:add','','1',NULL,'2020-02-04 09:40:51',NULL),(123,117,'修改文章','','article:update','','1',NULL,'2020-02-04 09:41:28',NULL),(124,117,'删除文章','','article:delete','','1',NULL,'2020-02-04 09:41:49',NULL),(125,167,'网站设置','admin/setting','setting:list','','0',NULL,'2020-02-09 01:30:18','2020-05-02 06:55:37'),(126,125,'修改设置','','setting:update','','1',NULL,'2020-02-09 01:31:17',NULL),(127,135,'单页管理','admin/singlePage','singlePage:list','','0',NULL,'2020-02-09 22:26:02','2020-02-10 08:44:17'),(128,127,'新增单页','','singlePage:add','','1',NULL,'2020-02-09 22:27:33','2020-02-09 22:32:45'),(129,127,'修改单页','','singlePage:update','','1',NULL,'2020-02-09 22:28:10','2020-02-09 22:33:01'),(130,127,'删除单页','','singlePage:delete','','1',NULL,'2020-02-09 22:28:32','2020-02-09 22:33:14'),(131,167,'导航管理','admin/navMenu','navMenu:list','','0',NULL,'2020-02-10 06:30:30','2020-05-05 19:22:33'),(132,131,'新增导航','','navMenu:add','','1',NULL,'2020-02-10 06:31:03',NULL),(133,131,'修改导航','','navMenu:update','','1',NULL,'2020-02-10 06:31:33',NULL),(134,131,'删除导航','','navMenu:delete','','1',NULL,'2020-02-10 06:31:55',NULL),(135,0,'内容管理','','','zmdi zmdi-archive','0',NULL,'2020-02-10 08:42:46','2020-02-24 10:00:14'),(136,135,'轮播图管理','admin/banner','banner:list','','0',NULL,'2020-02-17 04:27:07',NULL),(137,136,'新增轮播图','','banner:add','','1',NULL,'2020-02-17 04:27:49',NULL),(138,136,'修改轮播图','','banner:update','','1',NULL,'2020-02-17 04:28:14',NULL),(139,136,'删除轮播图','','banner:delete','','1',NULL,'2020-02-17 04:28:38',NULL),(140,135,'友情链接','admin/friendLink','friendLink:list','','0',NULL,'2020-02-23 00:42:33','2020-02-23 01:48:47'),(141,140,'新增链接','','friendLink:add','','1',NULL,'2020-02-23 00:45:14',NULL),(142,140,'修改链接','','friendLink:update','','1',NULL,'2020-02-23 00:45:47',NULL),(143,140,'删除链接','','friendLink:delete','','1',NULL,'2020-02-23 00:46:17',NULL),(145,135,'Tag管理','admin/articleTag','articleTag:list','','0',NULL,'2020-03-13 01:34:55','2020-03-13 01:42:42'),(146,145,'新增Tag','','articleTag:add','','1',NULL,'2020-03-13 01:37:28',NULL),(147,145,'修改Tag','','articleTag:update','','1',NULL,'2020-03-13 01:38:00','2020-03-13 01:38:41'),(148,145,'删除Tag','','articleTag:delete','','1',NULL,'2020-03-13 01:38:21','2020-03-13 01:38:57'),(150,135,'文章评论','admin/articleComment','articleComment:list','','0',NULL,'2020-04-21 21:51:09','2020-06-15 11:00:05'),(151,150,'新增','','articleComment:add','','1',NULL,'2020-04-21 21:52:57',NULL),(152,150,'更新','','articleComment:update','','1',NULL,'2020-04-21 21:53:28',NULL),(153,150,'删除','','articleComment:delete','','1',NULL,'2020-04-21 21:54:02',NULL),(154,0,'商城管理','','','mdi mdi-shopping','0',NULL,'2020-04-23 23:30:15','2020-05-08 01:24:11'),(155,154,'商品分类','admin/productCategory','productCategory:list','','0',NULL,'2020-04-23 23:31:44',NULL),(156,155,'增加分类','','productCategory:add','','1',NULL,'2020-04-23 23:32:47',NULL),(157,155,'更新分类','','productCategory:update','','1',NULL,'2020-04-23 23:33:12',NULL),(158,155,'删除分类','','productCategory:delete','','1',NULL,'2020-04-23 23:33:34',NULL),(159,154,'商品','admin/product','product:list','mdi mdi-basket','0',NULL,'2020-04-24 02:53:16',NULL),(160,159,'添加商品','','product:add','','1',NULL,'2020-04-24 02:53:46',NULL),(161,159,'修改商品','','product:update','','1',NULL,'2020-04-24 02:54:04',NULL),(162,159,'删除商品','','product:delete','','1',NULL,'2020-04-24 02:54:20',NULL),(163,154,'订单','admin/order','order:list','mdi mdi-border-color','0',NULL,'2020-04-28 10:23:02','2020-04-28 10:27:08'),(164,163,'新增订单','','order:add','','1',NULL,'2020-04-28 10:24:55',NULL),(165,163,'修改订单','','order:update','','1',NULL,'2020-04-28 10:25:12',NULL),(166,163,'删除订单','','order:delete','','1',NULL,'2020-04-28 10:25:30',NULL),(167,0,'系统设置','','','mdi mdi-settings-box','0',NULL,'2020-05-02 06:53:25','2020-05-02 06:53:57'),(168,167,'主题设置','admin/theme','theme:list','','0',NULL,'2020-05-02 06:54:59',NULL),(169,168,'更新主题','','theme:update','','1',NULL,'2020-05-02 06:56:26',NULL),(170,154,'专题','admin/topic','topic:list','mdi mdi-access-point','0',NULL,'2020-05-14 16:02:51',NULL),(171,170,'新增','','topic:add','','1',NULL,'2020-05-14 16:03:23',NULL),(172,170,'修改','','topic:update','','1',NULL,'2020-05-14 16:03:49',NULL),(173,170,'删除','','topic:delete','','1',NULL,'2020-05-14 16:04:07',NULL),(174,154,'优惠券','admin/coupon','coupon:list','','0',NULL,'2020-05-26 09:53:36',NULL),(175,174,'新增优惠券','','coupon:add','','1',NULL,'2020-05-26 09:54:12',NULL),(176,174,'修改优惠券','','coupon:update','','1',NULL,'2020-05-26 09:54:34',NULL),(177,174,'删除优惠券','','coupon:delete','','1',NULL,'2020-05-26 09:54:54',NULL),(178,154,'商品评论','admin/productComment','productComment:list','mdi mdi-access-point','0',NULL,'2020-06-15 10:59:37',NULL),(179,178,'删除评论','','productComment:delete','','1',NULL,'2020-06-15 11:00:56',NULL),(180,178,'更新评论','','productComment:update','','1',NULL,'2020-06-15 11:01:34',NULL);
/*!40000 ALTER TABLE `t_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_nav_menu`
--

DROP TABLE IF EXISTS `t_nav_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_nav_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `seq_num` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nav_url` varchar(100) DEFAULT NULL,
  `nav_status` char(1) DEFAULT NULL,
  `target` varchar(45) DEFAULT NULL COMMENT '打开方式',
  `nav_type` varchar(45) DEFAULT NULL COMMENT '关联内容类型 1.page 2.category 3.product',
  `relation_id` varchar(45) DEFAULT NULL COMMENT '关联模型的具体数据ID',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_nav_menu`
--

LOCK TABLES `t_nav_menu` WRITE;
/*!40000 ALTER TABLE `t_nav_menu` DISABLE KEYS */;
INSERT INTO `t_nav_menu` VALUES (1,0,'首页',1,1,'/index','1','_self','page','2','2020-02-10 06:41:09'),(26,0,'新闻',4,1,'/articleCategory/xinwenzhongxin','1','_self','category','14','2020-03-03 06:58:31'),(33,26,'公司动态',1,1,'/articleCategory/gongsidongtai','1','_self','category','20','2020-03-03 07:04:49'),(35,26,'行业动态',2,1,'/articleCategory/hengyedongtai','1','_self','category','21','2020-03-03 07:05:52'),(36,26,'精彩专题',3,1,'/articleCategory/jingcaizhuanti','0','_self','category','22','2020-03-03 07:06:29'),(51,0,'武术器材',1,1,'/productCategory/wushu','1','_self','productCategory','1','2020-04-24 00:32:19'),(52,51,'刀剑',2,1,'/productCategory/wushu_daojian','1','_self','productCategory','2','2020-04-24 00:43:55'),(56,51,'服装',2,1,'/productCategory/wushufu','1','_self','productCategory','4','2020-05-14 01:36:09'),(57,0,'双11专题',1,1,'/productTopic/doubleeleven','1','_self','productTopic','2','2020-05-15 08:40:43'),(64,0,'关于我们',7,NULL,'/page/guanyuwomen','1','_self','page','5','2020-06-13 14:05:10');
/*!40000 ALTER TABLE `t_nav_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product`
--

DROP TABLE IF EXISTS `t_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `slug` varchar(128) DEFAULT NULL COMMENT 'slug',
  `category_ids` varchar(100) DEFAULT NULL COMMENT '分类',
  `title` varchar(256) DEFAULT '' COMMENT '标题',
  `content` longtext COMMENT '内容',
  `summary` text COMMENT '摘要',
  `usp` text COMMENT '产品卖点',
  `thumbnail` varchar(512) DEFAULT NULL COMMENT '缩略图',
  `video` varchar(512) DEFAULT NULL COMMENT '视频',
  `video_cover` varchar(512) DEFAULT NULL,
  `template` varchar(50) DEFAULT NULL COMMENT '模板',
  `seq_num` int(11) DEFAULT '0' COMMENT '排序编号',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '商品的用户ID',
  `user_divide_type` int(11) DEFAULT NULL COMMENT '商品的销售分成类型：0平台所有，1用户所有，2按比例分成',
  `price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `origin_price` decimal(10,2) DEFAULT NULL COMMENT '原始价格',
  `limited_price` decimal(10,2) DEFAULT NULL COMMENT '限时优惠价（早鸟价）',
  `limited_time` datetime DEFAULT NULL COMMENT '限时优惠截止时间',
  `product_status` char(1) DEFAULT '1' COMMENT '状态',
  `comment_status` char(1) DEFAULT '1' COMMENT '评论状态，默认允许评论',
  `comment_count` int(11) unsigned DEFAULT '0' COMMENT '评论总数',
  `comment_time` datetime DEFAULT NULL COMMENT '最后评论时间',
  `view_count` int(11) unsigned DEFAULT '0' COMMENT '访问量',
  `sales_count` int(11) unsigned DEFAULT '0' COMMENT '销售量，用于放在前台显示',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_time` datetime DEFAULT NULL COMMENT '最后更新日期',
  `flag` varchar(256) DEFAULT NULL COMMENT '标识，通常用于对某几个商品进行标识，从而实现单独查询',
  `meta_keywords` varchar(512) DEFAULT NULL COMMENT 'SEO关键字',
  `meta_description` varchar(512) DEFAULT NULL COMMENT 'SEO描述信息',
  `remarks` text COMMENT '备注信息',
  `options` text COMMENT 'json字段扩展',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `user_id` (`user_id`),
  KEY `created` (`create_time`),
  KEY `view_count` (`view_count`),
  KEY `order_number` (`seq_num`),
  KEY `sales_count` (`sales_count`),
  KEY `status` (`product_status`),
  KEY `flag` (`flag`(191))
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product`
--

LOCK TABLES `t_product` WRITE;
/*!40000 ALTER TABLE `t_product` DISABLE KEYS */;
INSERT INTO `t_product` VALUES (1,'wushubiaoyandao','2,1','武术表演刀','<p style=\"text-align:center\"><span style=\"color:#c0392b\"><strong>商品详情介绍</strong></span></p> \n<p>关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到关工大到</p> \n<p><strong><img alt=\"武术表演\" src=\"/demoimgs/guangongdao.jpg\"></strong></p>','productsummary','','/demoimgs/guangongdao.jpg',NULL,NULL,'product.html',3,1,NULL,43.00,23.00,13.00,'2020-04-24 23:50:00','1','1',NULL,NULL,570,2,'2020-04-24 23:46:38','2020-06-13 17:22:24',NULL,'武术器材, 刀剑,表演刀','关工大到的seo描述','',NULL),(2,'huwaizhangpeng','3','户外帐篷','<p>户外帐篷户外帐篷户外帐篷户外帐篷户外帐篷户外帐篷户外帐篷户外帐篷户外帐篷</p>\r\n','productsummary','篷户外帐篷户外帐篷户外','/demoimgs/zhangpeng.jpg',NULL,NULL,'product.html',1,1,NULL,234.00,357.00,NULL,NULL,'1','1',NULL,NULL,59,0,'2020-05-14 01:41:04','2020-05-19 04:56:27',NULL,'户外帐篷','户外帐篷','',NULL),(3,'lansewushufu','4','武术服','<p>武术服武术服武术服武术服武术服</p>\r\n','productsummary','术服武术服武','/demoimgs/lansewushufu.jpg',NULL,NULL,'product.html',2,1,NULL,55.00,67.00,NULL,NULL,'1','1',NULL,NULL,65,1,'2020-05-14 02:25:00','2020-06-04 09:45:47',NULL,'武术服','','',NULL),(4,'denshanxie','3','登录鞋','<p>登录鞋登录鞋登录鞋登录鞋</p>\r\n','productsummary','鞋登录鞋登白色卖点了','/demoimgs/baisexiezi.png',NULL,NULL,'product.html',2,1,NULL,67.00,75.00,NULL,NULL,'1','1',NULL,NULL,20,0,'2020-05-14 02:29:28','2020-05-19 10:02:35',NULL,'','登录鞋','',NULL),(5,'wangyuanjing','3','望远镜','<p>望远镜望远镜望远镜望远镜望远镜望远镜望远镜望远镜望远镜</p>\r\n','productsummary','望远镜','/demoimgs/wangyuanjing.jpg',NULL,NULL,'product.html',3,1,NULL,234.00,431.00,NULL,NULL,'1','1',NULL,NULL,1,0,'2020-05-14 02:31:47','2020-05-14 02:31:47',NULL,'望远镜','望远镜','望远镜',NULL),(6,'baonuanfu','3','保暖服','<p>保暖服保暖服保暖服保暖服保暖服</p>\r\n','productsummary','暖服保暖服保暖服保暖服','/demoimgs/baonuanfu.jpg',NULL,NULL,'product.html',4,1,NULL,99.00,123.00,NULL,NULL,'1','1',NULL,NULL,4,0,'2020-05-14 02:36:23','2020-06-09 13:38:26',NULL,'服装,保暖服保暖服','保暖服保暖服','',NULL),(7,'quanjishoutao','4','拳击手套','<p>拳击手套拳击手套拳击手套</p>\r\n','productsummary','套拳击手套拳击卖点','/demoimgs/baonuanfu.jpg',NULL,NULL,'product.html',5,1,NULL,55.00,55.00,NULL,NULL,'1','1',NULL,NULL,94,0,'2020-05-14 03:26:01','2020-06-09 13:37:58',NULL,'服装,拳击手套','拳击手套','',NULL),(8,'gongluche','3','自行车','<p>自行车自行车自行车自行车自行车</p>\r ','productsummary','自行车自行车自行车自行车自行车卖点','/demoimgs/zixingchetou.png',NULL,NULL,'product.html',6,1,NULL,88.00,98.00,NULL,NULL,'1','1',NULL,NULL,13,0,'2020-05-14 03:28:17','2020-05-19 10:03:04',NULL,'记录仪','记录仪记录仪','',NULL),(9,'huwaiyizi','3','户外椅子','<p>户外椅子户外椅子户外椅子户外椅子户外椅子户外椅子户外椅子</p>\r\n','productsummary','子户外椅子户外椅卖点硬','/demoimgs/yiziyundong.png',NULL,NULL,'product.html',7,1,NULL,23.00,23.00,NULL,NULL,'1','1',NULL,NULL,40,0,'2020-05-14 03:31:03','2020-06-05 07:53:41',NULL,'户外椅子','户外椅子','',NULL),(10,'xiaobeibao','3','小背包','<p>小背包小背包小背包小背包小背包小背包</p>\r\n','productsummary','背包小背包小背包小','/demoimgs/xiaobeibao.jpg',NULL,NULL,'product.html',8,1,NULL,22.00,28.00,NULL,NULL,'1','1',NULL,NULL,41,0,'2020-05-14 03:33:16','2020-06-05 06:57:39',NULL,'小背包小背包','小背包小背包','',NULL),(11,'paobujiceshi','3,1,4','跑步机','<p>跑步机跑步机跑步机跑步机跑步机跑步机跑步机跑步机跑步机</p>\r\n\r\n<p>跑步机跑步机跑步机跑步机</p>\r\n','productsummary','我的跑步机卖点是快','/demoimgs/dianzi.jpg',NULL,NULL,'product.html',1,1,NULL,345.00,567.00,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,'2020-06-05 15:08:02','2020-06-09 13:41:40',NULL,'健身器械','跑步机跑步机','',NULL),(14,'sanshaoyedejian','2,1','三少爷的剑','<p>三少爷的剑三少爷的剑三少爷的剑</p>\r\n','三少爷的剑','三少爷的剑卖点是好用','/demoimgs/shenjian.png',NULL,NULL,'product.html',4,1,NULL,34.00,66.00,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,'2020-06-10 00:51:50','2020-06-10 01:03:16',NULL,'刀剑,武术器材','三少爷的剑三少爷的剑','',NULL),(15,'shadaixunlian','2,1','训练沙袋','<p>训练沙袋训练沙袋训练沙袋训练沙袋</p>\r\n','训练沙袋训练沙袋训练沙袋训练沙袋','沙袋训练沙袋训练沙袋训','/demoimgs/xunlianshadai.png',NULL,NULL,'product.html',2,1,NULL,35.00,35.00,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,'2020-06-13 11:32:10','2020-06-13 11:32:23',NULL,'刀剑,训练沙袋,武术器材','训练沙袋训练沙袋训练沙袋训练沙袋','',NULL),(16,'toukui','1,2,3','拳击头盔','<p>拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔拳击头盔</p>\r\n','拳击头盔','拳击头盔拳击头盔拳击头盔','/demoimgs/quajitou.png',NULL,NULL,'product.html',2,1,NULL,234.00,333.00,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,'2020-06-13 11:38:45','2020-06-13 11:38:45',NULL,'武术器材, 刀剑,拳击头盔','','拳击头盔拳击头盔拳击头盔',NULL);
/*!40000 ALTER TABLE `t_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_category`
--

DROP TABLE IF EXISTS `t_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级分类的ID',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '分类创建的用户ID',
  `slug` varchar(128) DEFAULT NULL COMMENT 'slug',
  `name` varchar(512) DEFAULT NULL COMMENT '分类名称',
  `content` text COMMENT '内容描述',
  `thumbnail` varchar(100) DEFAULT NULL COMMENT '分类缩略图',
  `summary` text COMMENT '摘要',
  `template` varchar(32) DEFAULT NULL COMMENT '模板',
  `nav_show` char(1) DEFAULT NULL COMMENT '是否显示在导航上 状态 0否 1是',
  `type` varchar(32) DEFAULT NULL COMMENT '类型，比如：分类、tag、专题',
  `icon` varchar(128) DEFAULT NULL COMMENT '图标',
  `counts` int(11) unsigned DEFAULT '0' COMMENT '该分类的内容数量',
  `seq_num` int(11) DEFAULT '0' COMMENT '排序编码',
  `flag` varchar(256) DEFAULT NULL COMMENT '标识',
  `meta_keywords` varchar(256) DEFAULT NULL COMMENT 'SEO关键字',
  `category_status` char(1) DEFAULT NULL COMMENT '状态 0锁定 1有效',
  `meta_description` varchar(256) DEFAULT NULL COMMENT 'SEO描述内容',
  `recommend` char(1) DEFAULT '0' COMMENT '推荐显示',
  `options` text,
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `typeslug` (`type`,`slug`),
  KEY `order_number` (`seq_num`),
  KEY `pid` (`parent_id`),
  KEY `flag` (`flag`(191))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品分类表。标签、专题、类别等都属于category。';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_category`
--

LOCK TABLES `t_product_category` WRITE;
/*!40000 ALTER TABLE `t_product_category` DISABLE KEYS */;
INSERT INTO `t_product_category` VALUES (1,0,1,'wushu','武术器材',NULL,NULL,NULL,'productList.html','1',NULL,NULL,NULL,1,NULL,'武术器','1','武术器材','0',NULL,'2020-04-24 00:32:19',NULL),(2,1,1,'wushu_daojian','刀剑',NULL,'/essence/img/bg-img/bg-2.jpg',NULL,'productList.html','1',NULL,NULL,NULL,2,NULL,'武术刀','1','武术刀','1',NULL,'2020-04-24 00:43:55',NULL),(3,0,1,'sportjianshen','运动装备',NULL,'/essence/img/bg-img/bg-4.jpg',NULL,'productList.html','0',NULL,NULL,NULL,2,NULL,'','1','2','1',NULL,'2020-05-01 09:15:17',NULL),(4,1,1,'wushufu','服装',NULL,'/essence/img/bg-img/bg-3.jpg',NULL,'productList.html','1',NULL,NULL,NULL,2,NULL,'武术类服装','1','武术类服装','1',NULL,'2020-05-14 01:36:09',NULL);
/*!40000 ALTER TABLE `t_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_category_mapping`
--

DROP TABLE IF EXISTS `t_product_category_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_category_mapping` (
  `product_id` int(11) unsigned NOT NULL COMMENT '商品ID',
  `category_id` int(11) unsigned NOT NULL COMMENT '分类ID',
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品和分类的多对多关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_category_mapping`
--

LOCK TABLES `t_product_category_mapping` WRITE;
/*!40000 ALTER TABLE `t_product_category_mapping` DISABLE KEYS */;
INSERT INTO `t_product_category_mapping` VALUES (1,1),(1,2),(2,3),(3,4),(4,3),(5,3),(6,3),(7,4),(8,3),(9,3),(10,3),(11,1),(11,3),(11,4),(12,1),(12,2),(14,1),(14,2),(15,1),(15,2),(16,1),(16,2),(16,3);
/*!40000 ALTER TABLE `t_product_category_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_comment`
--

DROP TABLE IF EXISTS `t_product_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` int(11) unsigned DEFAULT NULL COMMENT '回复的评论ID',
  `product_id` int(11) unsigned DEFAULT NULL COMMENT '评论的产品ID',
  `title` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '评论的用户ID',
  `author` varchar(128) DEFAULT NULL COMMENT '评论的作者',
  `content` text COMMENT '评论的内容',
  `reply_count` int(11) unsigned DEFAULT '0' COMMENT '评论的回复数量',
  `seq_num` int(11) DEFAULT '0' COMMENT '排序编号，常用语置顶等',
  `vote_up` int(11) unsigned DEFAULT '0' COMMENT '“顶”的数量',
  `vote_down` int(11) unsigned DEFAULT '0' COMMENT '“踩”的数量',
  `comment_status` tinyint(2) DEFAULT NULL COMMENT '评论的状态',
  `modify_uid` int(11) DEFAULT NULL COMMENT '更新或者审核用户的ID',
  `modify_username` varchar(100) DEFAULT NULL COMMENT '更新审核管管理员名称',
  `create_time` datetime DEFAULT NULL COMMENT '评论的时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`comment_status`),
  KEY `pid` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品评论表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_comment`
--

LOCK TABLES `t_product_comment` WRITE;
/*!40000 ALTER TABLE `t_product_comment` DISABLE KEYS */;
INSERT INTO `t_product_comment` VALUES (1,NULL,1,NULL,1,'admin','我就是来试试评论的',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 10:12:03',NULL),(2,NULL,1,NULL,1,'admin','fdsafdsafdsa又一个了',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 10:13:57',NULL),(3,NULL,1,NULL,1,'admin','fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了fdsafdsafdsa又一个了',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 10:28:33',NULL),(4,NULL,1,NULL,1,'admin','再重新发一条评论',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 10:29:29',NULL),(5,NULL,1,'武术表演刀',1,'admin','来一个待审核的',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 11:12:43',NULL),(6,NULL,1,'武术表演刀',1,'admin','我要感受一个这个如何',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 15:02:06',NULL),(7,NULL,1,'武术表演刀',1,'admin','我现在又重新发一个需要第五空间的',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-15 15:18:42',NULL),(8,NULL,1,'武术表演刀',1,'admin','据说不错，用来健身武术应该适用。',NULL,NULL,NULL,NULL,1,NULL,NULL,'2020-06-16 01:33:51',NULL),(9,NULL,1,'武术表演刀',173,'wtsoftware','我试试是不是要审核 的那种',NULL,8,NULL,NULL,1,1,'admin','2020-06-16 01:56:57','2020-06-16 01:57:15');
/*!40000 ALTER TABLE `t_product_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_image`
--

DROP TABLE IF EXISTS `t_product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `src` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `seq_num` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productid` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='产品图片表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_image`
--

LOCK TABLES `t_product_image` WRITE;
/*!40000 ALTER TABLE `t_product_image` DISABLE KEYS */;
INSERT INTO `t_product_image` VALUES (61,3,'/essence/img/product-img/Y8.jpg',0,'2020-06-04 09:45:47'),(62,3,'/essence/img/product-img/Y52.jpg',0,'2020-06-04 09:45:47'),(67,14,'/demoimgs/shenjian.png',0,'2020-06-10 01:03:16'),(73,15,'/demoimgs/xunlianshadai.png',1,'2020-06-13 18:25:25'),(74,15,'/demoimgs/dishadai.png',2,'2020-06-13 18:25:25'),(75,16,'/demoimgs/quajitou.png',1,'2020-06-13 18:25:25'),(76,16,'/demoimgs/quanjijing.png',3,'2020-06-13 18:25:25'),(77,9,'/demoimgs/yiziyundong.png',4,'2020-06-13 18:25:25'),(78,8,'/demoimgs/zixingchetou.png',1,'2020-06-13 18:25:25'),(79,8,'/demoimgs/zixingcheba.png',2,'2020-06-13 18:25:25'),(86,1,'/images/gallery/15.jpg',0,'2020-06-13 17:22:24'),(87,1,'/images/gallery/16.jpg',0,'2020-06-13 17:22:24'),(88,1,'/demoimgs/guangongdao.jpg',0,'2020-06-13 17:22:24');
/*!40000 ALTER TABLE `t_product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_sku`
--

DROP TABLE IF EXISTS `t_product_sku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品表的商品ID',
  `specification_ids` varchar(55) DEFAULT NULL COMMENT '规格ID组合',
  `specification` varchar(1023) NOT NULL COMMENT '商品规格值列表，采用JSON数组格式',
  `origin_price` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品货品价格',
  `stock` int(11) NOT NULL DEFAULT '0' COMMENT '商品货品数量',
  `img_url` varchar(125) DEFAULT NULL COMMENT '商品货品图片',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sku_unique` (`product_id`,`specification_ids`) COMMENT '一个商品每一个规格SKU信息只能有一个',
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=352 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品库存表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_sku`
--

LOCK TABLES `t_product_sku` WRITE;
/*!40000 ALTER TABLE `t_product_sku` DISABLE KEYS */;
INSERT INTO `t_product_sku` VALUES (282,2,'31','规格:S',67.00,44.00,33,'/demoimgs/zhangpeng.jpg','2020-05-19 04:56:31','2020-05-19 04:56:31'),(283,4,'37','规格:40',88.00,45.00,23,'','2020-05-19 10:02:35','2020-05-19 10:02:35'),(284,4,'38','规格:41',99.00,26.00,35,'','2020-05-19 10:02:35','2020-05-19 10:02:35'),(285,8,'39','规格:常规',99.00,89.00,32,'/demoimgs/timg (1).jpeg','2020-05-19 10:03:04','2020-05-19 10:03:04'),(298,3,'61,64','颜色:蓝,尺码:M',56.00,44.00,34,'','2020-06-04 09:45:47','2020-06-04 09:45:47'),(299,3,'62,64','颜色:白,尺码:M',56.00,36.00,50,'','2020-06-04 09:45:47','2020-06-04 09:45:47'),(301,10,'68','规格:常规',19.00,12.00,22,'','2020-06-05 06:57:39','2020-06-05 06:57:39'),(302,9,'69','规格:常规',33.00,23.00,12,'','2020-06-05 07:53:41','2020-06-05 07:53:41'),(314,7,'84','规格:常规',44.00,33.00,19,'/demoimgs/quajjishoutao.jpg','2020-06-09 13:37:58','2020-06-09 13:37:58'),(315,6,'85','尺寸:S',44.00,33.00,22,'/demoimgs/baonuanfu.jpg','2020-06-09 13:38:26','2020-06-09 13:38:26'),(316,6,'86','尺寸:M',66.00,44.00,33,'/demoimgs/baonuanfu.jpg','2020-06-09 13:38:26','2020-06-09 13:38:26'),(319,11,'89','颜色:黑',567.00,345.00,23,'','2020-06-09 13:41:40','2020-06-09 13:41:40'),(320,11,'90','颜色:银',578.00,467.00,34,'','2020-06-09 13:41:40','2020-06-09 13:41:40'),(321,12,'91','规格:标准',45.00,33.00,22,'','2020-06-10 00:41:33','2020-06-10 00:41:33'),(324,14,'94','规格:标准',45.00,33.00,21,'','2020-06-10 01:03:16','2020-06-10 01:03:16'),(336,15,'112','颜色:黑',44.00,33.00,22,'','2020-06-13 11:32:23','2020-06-13 11:32:23'),(337,15,'113','颜色:灰',55.00,44.00,33,'','2020-06-13 11:32:23','2020-06-13 11:32:23'),(338,15,'114','颜色:红',55.00,44.00,33,'','2020-06-13 11:32:23','2020-06-13 11:32:23'),(339,16,'115','颜色:黑',333.00,234.00,22,'','2020-06-13 11:38:45','2020-06-13 11:38:45'),(348,1,'130,132,135','规格:1.2米*1.5,重量:3KG,刀柄类型:木制',25.00,23.00,11,'/demoimgs/guangongdao.jpg','2020-06-13 17:22:24','2020-06-13 17:22:24'),(349,1,'130,133,136','规格:1.2米*1.5,重量:4KG,刀柄类型:钢制',43.00,28.00,22,'/demoimgs/guangongdao.jpg','2020-06-13 17:22:24','2020-06-13 17:22:24'),(350,1,'131,133,136','规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',32.00,24.00,13,'/demoimgs/guangongdao.jpg','2020-06-13 17:22:24','2020-06-13 17:22:24'),(351,1,'131,134,135','规格:1.8米*1.8,重量:5KG,刀柄类型:木制',35.00,29.00,4,'/demoimgs/guangongdao.jpg','2020-06-13 17:22:24','2020-06-13 17:22:24');
/*!40000 ALTER TABLE `t_product_sku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_specification`
--

DROP TABLE IF EXISTS `t_product_specification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品表的商品ID',
  `specification` varchar(255) NOT NULL DEFAULT '' COMMENT '商品规格名称',
  `value` varchar(255) NOT NULL DEFAULT '' COMMENT '商品规格值',
  `pic_url` varchar(255) NOT NULL DEFAULT '' COMMENT '商品规格图片',
  `product_status` char(1) DEFAULT '1' COMMENT '逻辑删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=347 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品规格表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_specification`
--

LOCK TABLES `t_product_specification` WRITE;
/*!40000 ALTER TABLE `t_product_specification` DISABLE KEYS */;
INSERT INTO `t_product_specification` VALUES (274,5,'规格','常规','',NULL,'2020-05-14 02:31:47','2020-05-14 02:31:47'),(296,2,'规格','S,M,L,XL','',NULL,'2020-05-19 04:56:31','2020-05-19 04:56:31'),(298,4,'规格','40,41','',NULL,'2020-05-19 10:02:35','2020-05-19 10:02:35'),(299,8,'规格','常规','',NULL,'2020-05-19 10:03:04','2020-05-19 10:03:04'),(309,3,'颜色','蓝,白','',NULL,'2020-06-04 09:45:47','2020-06-04 09:45:47'),(310,3,'尺码','S,M,L,XL','',NULL,'2020-06-04 09:45:47','2020-06-04 09:45:47'),(312,10,'规格','常规','',NULL,'2020-06-05 06:57:39','2020-06-05 06:57:39'),(313,9,'规格','常规','',NULL,'2020-06-05 07:53:41','2020-06-05 07:53:41'),(321,7,'规格','常规','',NULL,'2020-06-09 13:37:58','2020-06-09 13:37:58'),(322,6,'尺寸','S,M','',NULL,'2020-06-09 13:38:26','2020-06-09 13:38:26'),(324,11,'颜色','黑,银','',NULL,'2020-06-09 13:41:40','2020-06-09 13:41:40'),(325,12,'规格','标准','',NULL,'2020-06-10 00:41:33','2020-06-10 00:41:33'),(328,14,'规格','标准','',NULL,'2020-06-10 01:03:16','2020-06-10 01:03:16'),(336,15,'颜色','黑,灰,红','',NULL,'2020-06-13 11:32:23','2020-06-13 11:32:23'),(337,16,'颜色','黑','',NULL,'2020-06-13 11:38:45','2020-06-13 11:38:45'),(344,1,'规格','1.2米*1.5,1.8米*1.8','',NULL,'2020-06-13 17:22:24','2020-06-13 17:22:24'),(345,1,'重量','3KG,4KG,5KG','',NULL,'2020-06-13 17:22:24','2020-06-13 17:22:24'),(346,1,'刀柄类型','木制,钢制','',NULL,'2020-06-13 17:22:24','2020-06-13 17:22:24');
/*!40000 ALTER TABLE `t_product_specification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_specification_value`
--

DROP TABLE IF EXISTS `t_product_specification_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_specification_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `specification_id` int(11) DEFAULT NULL COMMENT '规格ID(t_product_specification表主键)',
  `spec_value` varchar(55) DEFAULT NULL,
  `seq_num` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_specification_value`
--

LOCK TABLES `t_product_specification_value` WRITE;
/*!40000 ALTER TABLE `t_product_specification_value` DISABLE KEYS */;
INSERT INTO `t_product_specification_value` VALUES (31,2,296,'S',1,NULL),(32,2,296,'M',2,NULL),(33,2,296,'L',3,NULL),(34,2,296,'XL',4,NULL),(37,4,298,'40',1,NULL),(38,4,298,'41',2,NULL),(39,8,299,'常规',1,NULL),(61,3,309,'蓝',1,'2020-06-04 09:45:47'),(62,3,309,'白',2,'2020-06-04 09:45:47'),(63,3,310,'S',1,'2020-06-04 09:45:47'),(64,3,310,'M',2,'2020-06-04 09:45:47'),(65,3,310,'L',3,'2020-06-04 09:45:47'),(66,3,310,'XL',4,'2020-06-04 09:45:47'),(68,10,312,'常规',1,'2020-06-05 06:57:39'),(69,9,313,'常规',1,'2020-06-05 07:53:41'),(84,7,321,'常规',1,'2020-06-09 13:37:58'),(85,6,322,'S',1,'2020-06-09 13:38:26'),(86,6,322,'M',2,'2020-06-09 13:38:26'),(89,11,324,'黑',1,'2020-06-09 13:41:40'),(90,11,324,'银',2,'2020-06-09 13:41:40'),(91,12,325,'标准',1,'2020-06-10 00:41:33'),(94,14,328,'标准',1,'2020-06-10 01:03:16'),(112,15,336,'黑',1,'2020-06-13 11:32:23'),(113,15,336,'灰',2,'2020-06-13 11:32:23'),(114,15,336,'红',3,'2020-06-13 11:32:23'),(115,16,337,'黑',1,'2020-06-13 11:38:45'),(130,1,344,'1.2米*1.5',1,'2020-06-13 17:22:24'),(131,1,344,'1.8米*1.8',2,'2020-06-13 17:22:24'),(132,1,345,'3KG',1,'2020-06-13 17:22:24'),(133,1,345,'4KG',2,'2020-06-13 17:22:24'),(134,1,345,'5KG',3,'2020-06-13 17:22:24'),(135,1,346,'木制',1,'2020-06-13 17:22:24'),(136,1,346,'钢制',2,'2020-06-13 17:22:24');
/*!40000 ALTER TABLE `t_product_specification_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_topic`
--

DROP TABLE IF EXISTS `t_product_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(45) DEFAULT NULL COMMENT 'slug',
  `topic_name` varchar(45) DEFAULT NULL,
  `template` varchar(50) DEFAULT NULL COMMENT '模板',
  `detail_template` varchar(50) DEFAULT NULL COMMENT '详情页模板',
  `thumbnail` varchar(100) DEFAULT NULL COMMENT '专题缩略图',
  `seo_keywords` varchar(255) DEFAULT NULL COMMENT 'seo关键字',
  `recommend` char(1) DEFAULT NULL COMMENT '是否首页推荐(1是0否)',
  `seo_descriptions` varchar(45) DEFAULT NULL COMMENT 'seo描述',
  `descriptions` varchar(255) DEFAULT NULL COMMENT '专题描述',
  `nav_show` char(1) DEFAULT '0' COMMENT '是否导航显示(1是0否)',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='商品专题(一个专题可以选择多个不同品类的商品)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_topic`
--

LOCK TABLES `t_product_topic` WRITE;
/*!40000 ALTER TABLE `t_product_topic` DISABLE KEYS */;
INSERT INTO `t_product_topic` VALUES (2,'doubleeleven','双11专题','topicProductList.html','product.html','/essence/img/bg-img/bg-5.jpg','双11专题','1','双11专题','双11专题','1','2020-05-15 00:23:26'),(3,'prodiscount','折扣专题','topicProductList.html','product.html','/essence/img/bg-img/bg-8.jpg','折扣专题','1','折扣专题','折扣专题','0','2020-05-28 09:20:33');
/*!40000 ALTER TABLE `t_product_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_product_topic_mapping`
--

DROP TABLE IF EXISTS `t_product_topic_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_product_topic_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL COMMENT '专题ID',
  `product_id` int(11) DEFAULT NULL COMMENT '商品ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tpunix` (`topic_id`,`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='专题和商品关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_product_topic_mapping`
--

LOCK TABLES `t_product_topic_mapping` WRITE;
/*!40000 ALTER TABLE `t_product_topic_mapping` DISABLE KEYS */;
INSERT INTO `t_product_topic_mapping` VALUES (10,2,1),(6,2,2),(1,2,3),(8,2,7),(9,2,10),(11,3,8),(12,3,9),(13,3,10);
/*!40000 ALTER TABLE `t_product_topic_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_role`
--

DROP TABLE IF EXISTS `t_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_role` (
  `ROLE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `ROLE_NAME` varchar(100) NOT NULL COMMENT '角色名称',
  `REMARK` varchar(100) DEFAULT NULL COMMENT '角色描述',
  `CREATE_TIME` datetime NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_role`
--

LOCK TABLES `t_role` WRITE;
/*!40000 ALTER TABLE `t_role` DISABLE KEYS */;
INSERT INTO `t_role` VALUES (1,'admin','超级管理员','2017-12-27 16:23:11','2020-06-15 11:04:44'),(2,'user','注册用户','2018-09-04 16:23:09','2020-05-12 22:35:05'),(4,'manager','普通管理员','2020-06-02 16:00:51','2020-06-02 16:06:12');
/*!40000 ALTER TABLE `t_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_role_menu`
--

DROP TABLE IF EXISTS `t_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_role_menu` (
  `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID',
  `MENU_ID` bigint(20) NOT NULL COMMENT '菜单/按钮ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_role_menu`
--

LOCK TABLES `t_role_menu` WRITE;
/*!40000 ALTER TABLE `t_role_menu` DISABLE KEYS */;
INSERT INTO `t_role_menu` VALUES (4,3),(4,117),(4,118),(4,127),(4,136),(4,1),(4,135),(4,154),(4,159),(1,2),(1,3),(1,67),(1,1),(1,4),(1,5),(1,6),(1,20),(1,21),(1,22),(1,10),(1,8),(1,66),(1,11),(1,12),(1,64),(1,13),(1,14),(1,65),(1,15),(1,16),(1,17),(1,18),(1,23),(1,19),(1,24),(1,101),(1,102),(1,103),(1,104),(1,105),(1,106),(1,107),(1,108),(1,109),(1,110),(1,113),(1,114),(1,117),(1,118),(1,119),(1,120),(1,121),(1,122),(1,123),(1,124),(1,125),(1,126),(1,127),(1,128),(1,129),(1,130),(1,131),(1,132),(1,133),(1,134),(1,135),(1,136),(1,137),(1,138),(1,139),(1,140),(1,141),(1,142),(1,143),(1,145),(1,146),(1,147),(1,148),(1,150),(1,151),(1,152),(1,153),(1,154),(1,155),(1,156),(1,157),(1,158),(1,159),(1,160),(1,161),(1,162),(1,163),(1,164),(1,165),(1,166),(1,167),(1,168),(1,169),(1,170),(1,171),(1,172),(1,173),(1,174),(1,175),(1,176),(1,177),(1,178),(1,179),(1,180);
/*!40000 ALTER TABLE `t_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_settings`
--

DROP TABLE IF EXISTS `t_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_key` varchar(100) DEFAULT NULL,
  `t_value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_settings`
--

LOCK TABLES `t_settings` WRITE;
/*!40000 ALTER TABLE `t_settings` DISABLE KEYS */;
INSERT INTO `t_settings` VALUES (1,'theme','essence'),(2,'version','1.1'),(3,'site_name','Febs内容管理'),(4,'domain','www.test.com'),(5,'site_logo','/upload/case6-1585580160212.jpg'),(7,'site_copyright','版本归febs所有'),(8,'site_phone','010-68676542'),(9,'site_email','rewrewfd@123.com'),(10,'site_icp','Copyright © 2016-2018 ICP 备888888号'),(12,'site_address','<strong>天使大厦,</strong> <span>海淀区海淀大街27</span>');
/*!40000 ALTER TABLE `t_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_single_page`
--

DROP TABLE IF EXISTS `t_single_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_single_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(120) DEFAULT NULL COMMENT 'slug',
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `seq_num` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `template` varchar(100) DEFAULT NULL,
  `page_status` char(1) DEFAULT NULL,
  `nav_show` char(1) DEFAULT NULL COMMENT '是否显示在导航上 状态 0否 1是',
  `summary` text COMMENT '内容摘要',
  `content` text,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pinyin_name_UNIQUE` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_single_page`
--

LOCK TABLES `t_single_page` WRITE;
/*!40000 ALTER TABLE `t_single_page` DISABLE KEYS */;
INSERT INTO `t_single_page` VALUES (2,'首页','index','index,keywords','index描述','admin',2,NULL,'index.html','1',NULL,NULL,'<p>/html/newsDetail.html/html/newsDetail.html/html/newsDetail.html/html/newsDetail.html/html/newsDetail.html/html/newsDetail.html</p>','2020-02-10 02:43:59'),(5,'关于我们','guanyuwomen','jonausre','91年12月由河北人民出版社出版发行','admin',7,NULL,'page.html','1','1',NULL,'<p>《中华武术器械大全》1991年12月由河北人民出版社出版发行。本书以早期青铜兵器与宋代前后的各种兵器为基础，以近代兵器与现代《中华武术器械大全》</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n\r\n<p>《中华武术器械大全》</p>\r\n\r\n<p>剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种。剑、刀、枪、棍棒、戟、叉、拐、斧、钺等进行系统编绘，共收有各种古兵器和武术器械1200余种</p>\r\n','2020-03-03 07:13:28'),(7,'登录','denglu','登录','登录','admin',5,NULL,'login.html','1','0',NULL,'<p>登录</p>\r\n','2020-03-06 06:05:20'),(8,'注册','zhuce','注册','注册','admin',7,NULL,'register.html','1','0',NULL,'<p>注册</p>\r\n','2020-03-06 06:12:42'),(9,'隐藏协议','yinsixieyi','隐藏协议','隐藏协议隐藏协议隐藏协议','admin',1,NULL,'page.html','1','0',NULL,'<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">一、</span><strong><span style=\"font-size:18px\">我们收集哪些信息以及如何使用信息</span></strong></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（一）您须授权我们收集和使用您个人信息的情形</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">收集个人信息的目的在于向您提供产品和/或服务，并且保证我们遵守适用的相关法律、法规及其他规范性文件。您有权自行选择是否提供该信息，但多数情况下，如果您不提供，我们可能无法向您提供相应的服务，也无法回应您遇到的问题。这些功能包括：</span></span></span></p>\r\n\r\n<p style=\"margin-left:24px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">实现网上购物所必须的功能</span></span></span></p>\r\n\r\n<p style=\"margin-left:48px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（1）&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">商品信息展示和搜索</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">为了让您快速地找到您所需要的商品，我们可能会收集您使用我们的产品与/或服务的设备信息（包括设备型号、设备标识、操作系统和小米商城的版本信息、语言设置、手机屏幕尺寸及分辨率、运营商信息）、浏览器类型来为您提供商品信息展示的最优方式。我们也会为了不断改进和优化上述的功能来使用您的上述个人信息。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">您也可以通过搜索来精准地找到您所需要的商品或服务。我们会保留您的搜索内容以方便您重复输入或为您展示与您搜索内容相关联的商品。请您注意，您的搜索关键词信息无法单独识别您的身份，不属于您的个人信息，我们有权以任何的目的对其进行使用；只有当您的搜索关键词信息与您的其他信息相互结合使用并可以识别您的身份时，则在结合使用期间，我们会将您的搜索关键词信息作为您的个人信息，与您的搜索历史记录一同按照本隐私政策对其进行处理与保护。</span></span></span></p>\r\n\r\n<p style=\"margin-left:48px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（2）&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">下单</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">当您准备对您购物车内的商品进行结算时，系统会生成您购买该商品的订单。您需要在订单中至少填写您的<strong>收货人姓名、收货地址以及手机号码</strong>，同时该订单中会载明<strong>订单号、您所购买的商品或服务信息、您应支付的货款金额及支付方式</strong>；您可以另外填写收货人的<strong>固定电话、邮箱地址信息</strong>以增加更多的联系方式确保商品可以准确送达，但不填写这些信息不影响您订单的生成。此外，购买物品的清单用于打印发票，并使您能够确定包裹中的物品。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">您在小米商城上购买手机号卡或合约机、充值话费、申请节能补贴等业务时，您还可能需要根据国家法律法规或服务提供方（包括基础电信运营商、移动转售运营商等）的要求提供您的实名信息，这些实名信息可能包括您的身份信息（比如您的<strong>身份证、军官证、工作居住证、护照、驾驶证等载明您身份的证件复印件或号码</strong>）、<strong>您本人的照片或视频、姓名、电话号码</strong>等。当您购买全屋定制服务时，我们还会收集您主动提供的<strong>房屋地址、姓名、电话</strong>、新媒体账号（选填）用于与您联系上门时间和地点，以及户型、房屋状态、采购预算信息用于为您定制方案。这些订单中将可能包含您的设备安装地址（可能是您的家庭地址）等信息。您在购买手机意外险的时候，我们还需要您提供手机IMEI。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">上述所有信息构成您的“订单信息”。若您购买的商品或服务属于小米商城自营的，为帮助您实现订单相关服务，我们将使用您的订单信息来进行您的身份核验、确定交易、支付结算、完成配送、为您查询和展示订单详情以及提供客服咨询与售后服务。若您购买的商品或服务由第三方商家直接提供，且商品的配送、客服与售后服务由此类商家为您提供，则我们会将您的订单信息提供给此类商家来实现此类目的。若第三方商家委托小米商城提供上述的配送、客服与售后服务，我们也会使用您的订单信息来为您提供此类服务。</span></span></span></p>\r\n\r\n<p style=\"margin-left:48px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（3）&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">支付功能</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">在您下单后，您可以选择小米商城的关联方或与小米商城合作的第三方支付机构（包括小米支付、微信支付、支付宝及银联等支付通道，以下称“支付机构”）所提供的支付服务。支付功能本身并不收集您的个人信息，但我们需要将您的订单号与交易金额信息与这些支付机构共享以实现其确认您的支付指令并完成支付。</span></span></span></p>\r\n\r\n<p style=\"margin-left:48px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（4）&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">交付产品或服务功能</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">在当您下单并选择货到付款或在线完成支付后，与小米商城合作的第三方配送公司（包括顺丰等，以下称“配送公司”）将为您完成订单的交付。您知晓并同意与小米商城合作的第三方配送公司会在上述环节内使用您的订单信息以保证您的订购的商品能够安全送达。</span></span></span></p>\r\n\r\n<p style=\"margin-left:48px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（5）&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">客服与售后功能</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">我们的电话客服和售后功能会使用您的账号信息和订单信息。为保证您的账号安全，我们的呼叫中心客服和在线客服会使用您的账号信息与您核验您的身份。当您需要我们提供与您订单信息相关的客服与售后服务时，我们将会查询您的订单信息。您有可能会在与我们的客服人员沟通时，提供给出上述信息外的其他信息，如当您要求我们变更配送地址、联系人或联系电话。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">您可以通过您设备的IMEI或SN码来查询和申请商品的售后服务。</span></span></span></p>\r\n\r\n<p style=\"margin-left:28px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"font-size:16px\">保障交易安全所必须的功能</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">为提高您使用我们的产品与/或服务时系统的安全性，更准确地预防钓鱼网站欺诈和保护账户安全，我们可能会通过了解您的账号信息、浏览信息、订单信息、您常用的软件信息、设备信息等手段来判断您的账号风险，并可能会记录一些我们认为有风险的链接（“URL”）；我们也会收集您的设备信息（MAC地址、客户端的IP地址、蓝牙开关状态）打击刷单等不正当购买行为。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（二）您可选择是否授权我们收集和使用您的个人信息的情形</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">1</span><span style=\"font-size:16px\">、为使您购物更便捷或更有乐趣，从而提升您在小米商城的网上购物体验，我们的以下附加功能中可能会收集和使用您的个人信息。如果您不提供这些个人信息，您依然可以进行网上购物，但您可能无法使用这些可以为您所带来购物乐趣的附加功能或在购买某些商品时需要重复填写一些信息。这些附加功能包括：</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（1）基于位置信息的附加功能：我们会收集您的位置信息（我们仅收集您当时所处的地理位置，但不会将您各时段的位置信息进行结合以判断您的行踪轨迹）来判断您所处的地点，自动为您推荐您所在区域可以购买的商品或服务；用来计算所在区域是否有区域库存；用于计算所在城市的米家和售后服务网店的距离；新建和修改地址时帮助您快速选择当前地址。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（2）基于摄像头（相机）的附加功能：您可以使用这个附加功能实现扫一扫、录制视频商品评价、录制短视频内容、通过拍摄照片/视频向客服描述问题的功能。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（3）基于图片上传的附加功能：您可以在小米商城上传图片商品评价、通过图片向客服描述的问题、上传短视频内容。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（4）基于通讯录信息的附加功能：我们将收集您的通讯录信息以方便您在购物时不再手动输入您通讯录中联系人的信息（比如您可以直接为通讯录中的电话号码充值）。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">（5）基于麦克风的附加功能：您可以使用这个附加功能完成录制视频商品评价、录制短视频内容、通过视频向客服描述问题。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">2</span><span style=\"font-size:16px\">、上述附加功能可能需要您在您的设备中向我们开启您的地理位置（位置信息）、相机（摄像头）、相册（图片库）、麦克风以及通讯录的访问权限，以实现这些功能所涉及的信息的收集和使用。请您注意，您开启这些权限即代表您授权我们可以收集和使用这些个人信息来实现上述的功能，您关闭权限即代表您取消了这些授权，则我们将不再继续收集和使用您的这些个人信息，也无法为您提供上述与这些授权所对应的功能。您关闭权限的决定不会影响此前基于您的授权所进行的个人信息的处理。</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">3</span><span style=\"font-size:16px\">、个性化推荐</span></span></span></p>\r\n\r\n<p style=\"margin-left:16px; margin-right:16px; text-align:justify\"><span style=\"font-size:14px\"><span style=\"color:#444444\"><span style=\"font-size:16px\">为了给您带来个性化的商品推荐和展示，我们会收集相关数据，通过算法将您感兴趣的商品或服务信息展示给您；或在您搜索时向您展示您可能希望找到的商品。为此，我们可能会收集您的地理位置、设备机型、搜索内容、购物车商品列表、订单信息、浏览信息等信息。对于从您的各种设备上收集到的信息，我们可能会将它们进行关联，以便我们能在这些设备上为您提供一致的服务。我们可能会将来自某项服务的信息与来自其他服务的信息结合起来，以便为您提供服务、个性化内容和建议。您可以随时在设置中关闭此功能。关于更多此类信息，请查看下文的“您的权利”部分。</span></span></span></p>\r\n','2020-06-13 14:08:55');
/*!40000 ALTER TABLE `t_single_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `status` char(1) NOT NULL COMMENT '状态 0锁定 1有效',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近访问时间',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `ssex` char(1) DEFAULT NULL COMMENT '性别 0男 1女',
  `theme` varchar(10) DEFAULT NULL COMMENT '主题',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user`
--

LOCK TABLES `t_user` WRITE;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` VALUES (1,'admin','$2a$10$WRUqPfZUAhRgwaiuBPE0XeUoPZKFgGTkRjaU04dHnL99leE4kBv1W',0,'wtsoftware@163.com','17788888888','1','2018-08-22 11:37:47','2020-03-26 02:13:51','2018-08-27 21:27:18',0.00,'0','green','20180414165840.jpg','我是创始人'),(2,'scott','$2a$10$vtf0gsIpFnzs3x9kA/2fW.oV41aAXFt4e4S0vBURGuFdLJ5F7xx2W',2,'scott@qq.com','','1','2018-08-22 11:37:47','2020-06-10 09:42:59','2018-08-27 21:27:18',0.00,'0','red','20180414170003.jpg',''),(173,'wtsoftware','$2a$10$C8djWfbKEjNK8hQVfu0bUekwvICHI.b5rnfhxYuSQqLfMVxxllJkm',11,'wtsoftware@163.com',NULL,'1','2020-04-27 00:48:57',NULL,NULL,0.00,'2','green','default.jpg',NULL);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_address`
--

DROP TABLE IF EXISTS `t_user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `contact_name` varchar(100) DEFAULT NULL COMMENT '联系人',
  `contact_mobile` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `zipcode` varchar(45) DEFAULT NULL COMMENT '邮编',
  `province` varchar(45) DEFAULT NULL COMMENT '省',
  `city` varchar(45) DEFAULT NULL COMMENT '市 ',
  `districts` varchar(45) DEFAULT NULL COMMENT '区县',
  `addr_details` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户个人地址信息表(一个用户存在多个快递信息)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_address`
--

LOCK TABLES `t_user_address` WRITE;
/*!40000 ALTER TABLE `t_user_address` DISABLE KEYS */;
INSERT INTO `t_user_address` VALUES (1,1,'自由','13781493310','10042','北京市','北京市市辖区','东城区','东四胡同3号','2020-05-12 03:11:00');
/*!40000 ALTER TABLE `t_user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_favourite`
--

DROP TABLE IF EXISTS `t_user_favourite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL COMMENT '收藏类型(product,article)',
  `title` varchar(100) DEFAULT NULL COMMENT '内容标题',
  `favourite_id` int(11) DEFAULT NULL COMMENT '所收藏的内容的主键ID',
  `user_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='用户收藏';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_favourite`
--

LOCK TABLES `t_user_favourite` WRITE;
/*!40000 ALTER TABLE `t_user_favourite` DISABLE KEYS */;
INSERT INTO `t_user_favourite` VALUES (1,'product','关工大刀',1,1,'2020-05-21 15:35:46'),(2,'product','武术服',3,1,'2020-06-01 13:17:17'),(6,'article','名牌工厂店1',12,1,'2020-06-02 15:02:02');
/*!40000 ALTER TABLE `t_user_favourite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_money_log`
--

DROP TABLE IF EXISTS `t_user_money_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_money_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL COMMENT '变动金额',
  `type` int(11) DEFAULT NULL COMMENT '交易类型(1.充值2.购物)',
  `content` varchar(45) DEFAULT NULL,
  `orderId` int(11) DEFAULT NULL COMMENT '如果是消费，则是对应的订单号',
  `recharge_type` int(11) DEFAULT NULL COMMENT '充值类型1.微信2支付宝3网银',
  `recharge_no` varchar(200) DEFAULT NULL COMMENT '如果是充值，则记录充值的流水号',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户资金交易记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_money_log`
--

LOCK TABLES `t_user_money_log` WRITE;
/*!40000 ALTER TABLE `t_user_money_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_user_money_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_order`
--

DROP TABLE IF EXISTS `t_user_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ns` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `product_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品的类型',
  `order_title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '商品的名称',
  `order_summary` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buyer_id` int(11) unsigned DEFAULT NULL COMMENT '购买人',
  `buyer_nickname` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '购买人昵称',
  `buyer_msg` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户留言',
  `delivery_fee` decimal(10,2) DEFAULT NULL COMMENT '快递费',
  `order_total_amount` decimal(10,2) DEFAULT NULL COMMENT '订单总金额，购买人员应该付款的金额(包含快递费delivery_fee)',
  `order_real_amount` decimal(10,2) DEFAULT NULL COMMENT '订单的真实金额，销售人员可以在后台修改支付金额，一般情况下 order_real_amount = order_total_amount',
  `coupon_user_id` int(11) DEFAULT NULL COMMENT '优惠券ID',
  `coupon_amount` decimal(10,2) DEFAULT NULL COMMENT '优惠金额',
  `pay_status` tinyint(2) DEFAULT NULL COMMENT '支付状态：1未付款、 2支付宝、3微信支付 、4网银（线上支付)5、余额支付',
  `pay_success_amount` decimal(10,2) DEFAULT NULL COMMENT '支付成功的金额',
  `pay_success_time` datetime DEFAULT NULL COMMENT '支付时间',
  `pay_success_proof` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付证明，手动入账时需要截图',
  `pay_success_remarks` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付备注',
  `payment_id` int(11) unsigned DEFAULT NULL COMMENT '支付记录',
  `payment_outer_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '第三方订单号',
  `payment_outer_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '第三方支付用户，一般情况下是用户的openId',
  `delivery_addr_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收货人地址',
  `delivery_addr_mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收货人手机号（电话）',
  `delivery_addr_province` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人省',
  `delivery_addr_city` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人的城市',
  `delivery_addr_district` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人的区（县）',
  `delivery_addr_detail` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人的详细地址',
  `delivery_addr_zipcode` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人地址邮政编码',
  `invoice_id` int(11) unsigned DEFAULT NULL COMMENT '发票',
  `invoice_status` tinyint(2) DEFAULT NULL COMMENT '发票开具状态：1 未申请发票、 2 发票申请中、 3 发票开具中、 8 无需开具发票、 9发票已经开具',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '管理员后台备注',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'json字段扩展',
  `trade_status` tinyint(2) DEFAULT NULL COMMENT '交易状态：1交易中、 2交易完成（但是可以申请退款） 、3取消交易 、4申请退款、 5拒绝退款、 6退款中、 7退款完成、 9交易结束',
  `del_status` tinyint(2) DEFAULT NULL COMMENT '删除状态：1 正常 ，2 回收站 3 已经删除',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ns` (`ns`),
  KEY `buyer_id` (`buyer_id`),
  KEY `payment_id` (`payment_id`),
  KEY `buyer_status` (`buyer_id`,`trade_status`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_order`
--

LOCK TABLES `t_user_order` WRITE;
/*!40000 ALTER TABLE `t_user_order` DISABLE KEYS */;
INSERT INTO `t_user_order` VALUES (1,'fdsafdas','prdouct','大刀','大刀管理员更新订单状态为拒绝退款,备注:null<br/>管理员更新订单状态为拒绝退款,备注:不好意思已经发货了。<br/>2020-06-02 18:39:00管理员更新订单状态为退款中,备注:好的，我已经在准备退款,完成以后，我会更新状态<br/>2020-06-02 19:12:44管理员更新订单状态为交易完成\n <br/>2020-06-02 19:13:13用户取消了订单备注:我突然不想要了，退钱吧。谢谢\n <br/>',1,'admin','练红手',NULL,222.00,222.00,NULL,22.00,3,22.00,NULL,NULL,'sdfsdafsda',NULL,NULL,NULL,'张先生','18347826819','北京市','北京市','石景山区','八宝山街道老山小区7号楼403','104001',1,1,'我着急使用，请尽快发货，感谢店家',NULL,4,0,'2020-06-02 11:12:44','2020-05-07 09:27:18'),(24,'OC464594334789152768',NULL,'关工大刀 ','null2020-06-02 18:39:47管理员更新订单状态为交易中,备注:<br/>2020-06-02 18:39:56用户取消了订单\n <br/>',1,'admin','试用一下这个订单优惠',10.00,58.00,10.00,5,50.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'自由','13781493310','北京市','北京市市辖区','东城区','东四胡同3号','10042',NULL,1,NULL,NULL,3,NULL,'2020-06-02 10:39:48','2020-05-31 14:13:07'),(25,'NA464605843908997120',NULL,'武术服 ','null2020-06-02 18:58:13管理员更新订单状态为交易中\n <br/>2020-06-02 19:06:18用户取消了订单\n <br/>2020-06-02 19:07:05管理员更新订单状态为交易中\n <br/>2020-06-02 19:10:02用户取消了订单\n <br/>2020-06-02 19:10:39管理员更新订单状态为交易中\n <br/>2020-06-02 19:10:50用户取消了订单备注:我真的是型号选错了\n <br/>2020-06-02 19:11:02管理员更新订单状态为交易中\n <br/>',1,'admin','',10.00,46.00,46.00,NULL,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'自由','13781493310','北京市','北京市市辖区','东城区','东四胡同3号','10042',NULL,1,NULL,NULL,1,NULL,'2020-06-02 11:11:03','2020-05-31 14:58:51'),(26,'AD465536012626702336',NULL,'关工大刀 ',NULL,1,'admin','',10.00,34.00,34.00,NULL,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'自由','13781493310','北京市','北京市市辖区','东城区','东四胡同3号','10042',NULL,1,NULL,NULL,1,NULL,'2020-06-03 04:35:00','2020-06-03 04:35:00'),(27,'XI465663656496214016',NULL,'记录仪 ',NULL,1,'admin',NULL,10.00,99.00,99.00,NULL,0.00,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,2,NULL,'2020-06-13 09:04:34','2020-06-03 13:02:13'),(28,'KY465663656496214016',NULL,'记录仪 ',NULL,1,'admin',NULL,10.00,99.00,99.00,NULL,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,1,NULL,'2020-06-03 13:02:13','2020-06-03 13:02:13'),(29,'PE466684007569174528',NULL,'关工大刀 ','null2020-06-06 16:50:55管理员更新订单状态为交易完成\n <br/>',1,'admin','',10.00,58.00,58.00,NULL,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'自由','13781493310','北京市','北京市市辖区','东城区','东四胡同3号','10042',NULL,1,NULL,NULL,2,NULL,'2020-06-06 08:50:56','2020-06-06 08:36:44'),(30,'YZ468590094136389632',NULL,'三少爷的剑 ',NULL,1,'admin','好好包装一下',10.00,43.00,43.00,NULL,0.00,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'自由','13781493310','北京市','北京市市辖区','东城区','东四胡同3号','10042',NULL,1,NULL,NULL,9,NULL,'2020-06-11 14:50:50','2020-06-11 14:50:50'),(31,'KT468608572834721792',NULL,'武术服 ','2020-06-12 00:05:33用户确认收货，状态变更为交易结束\n <br/>2020-06-12 00:05:49管理员更新订单状态为交易结束\n <br/>',1,'admin','',10.00,46.00,46.00,NULL,0.00,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'自由','13781493310','北京市','北京市市辖区','东城区','东四胡同3号','10042',NULL,1,NULL,NULL,9,NULL,'2020-06-11 16:05:50','2020-06-11 16:04:16');
/*!40000 ALTER TABLE `t_user_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_order_delivery`
--

DROP TABLE IF EXISTS `t_user_order_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_order_delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作用户名',
  `user_id` int(11) DEFAULT NULL COMMENT '操作用户ID',
  `order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `delivery_id` int(11) DEFAULT NULL COMMENT '快递公司ID',
  `company` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '快递公司',
  `delivery_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '快递单号',
  `start_time` datetime DEFAULT NULL COMMENT '快递发货时间',
  `finish_time` datetime DEFAULT NULL COMMENT '快递送达时间',
  `addr_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收货人地址',
  `addr_mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收货人手机号（电话）',
  `addr_province` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人省',
  `addr_city` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人的城市',
  `addr_district` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人的区（县）',
  `addr_detail` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人的详细地址',
  `addr_zipcode` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收件人地址邮政编码',
  `delivery_status` tinyint(2) DEFAULT NULL COMMENT '发货状态1.已发货(待运输)2.运输中3.已收货',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '运输信息备注',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'json字段扩展',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='发货信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_order_delivery`
--

LOCK TABLES `t_user_order_delivery` WRITE;
/*!40000 ALTER TABLE `t_user_order_delivery` DISABLE KEYS */;
INSERT INTO `t_user_order_delivery` VALUES (3,'admin',1,1,2,'申通快递','ST001234','2018-08-27 21:27:18','2018-08-27 21:27:18','张先生','18347826819','北京市','北京市','石景山区','八宝山街道老山小区7号楼403','104001',4,'64G的A供商发出',NULL,'2018-08-27 21:27:18','2018-08-27 21:27:18'),(7,'admin',1,NULL,1,NULL,'SF00567','2020-05-07 23:53:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'FFFFTESTFDS',NULL,NULL,'2020-05-07 23:53:15'),(8,'admin',1,NULL,1,NULL,'SF00111','2020-05-07 23:54:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ffffffdsafdsafdsa',NULL,NULL,'2020-05-07 23:54:34'),(10,'admin',1,NULL,NULL,NULL,NULL,'2020-05-08 00:07:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-08 00:07:29'),(11,'admin',1,NULL,NULL,NULL,NULL,'2020-05-08 00:08:26',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-05-08 00:08:26'),(13,'admin',1,1,1,'顺丰快递','SF00678234','2020-05-13 03:32:25',NULL,'张先生','18347826819','北京市','北京市','石景山区','八宝山街道老山小区7号楼403','104001',2,'一付耳机',NULL,NULL,'2020-05-13 03:32:25'),(15,'admin',1,30,1,'顺丰快递','SF0023455','2020-06-11 15:22:02',NULL,'自由','13781493310','北京市','北京市','东城区','东四胡同3号','10042',4,'用户已经 签收',NULL,NULL,'2020-06-11 15:22:02'),(16,'admin',1,31,1,'顺丰快递','SF0013434','2020-06-11 16:05:23',NULL,'自由','13781493310','北京市','北京市','东城区','东四胡同3号','10042',4,'用户已签收',NULL,NULL,'2020-06-11 16:05:23');
/*!40000 ALTER TABLE `t_user_order_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_order_invoice`
--

DROP TABLE IF EXISTS `t_user_order_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_order_invoice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票类型(普通发票、增值税专用发票)',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票抬头',
  `content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票内容',
  `identity` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纳税人识别号',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '单位名称',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票收取人手机号',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票收取人邮箱',
  `invoice_status` tinyint(2) DEFAULT NULL COMMENT '发票状态',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'json字段扩展',
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='发票信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_order_invoice`
--

LOCK TABLES `t_user_order_invoice` WRITE;
/*!40000 ALTER TABLE `t_user_order_invoice` DISABLE KEYS */;
INSERT INTO `t_user_order_invoice` VALUES (1,'普通发票','阳光科技发展有限wcng','体育健身器材','LXN43432432432DE','阳光科技发展有限公司','13781403318','2345@123.com',1,NULL,'2018-08-27 21:27:18','2018-08-27 21:27:18');
/*!40000 ALTER TABLE `t_user_order_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_order_item`
--

DROP TABLE IF EXISTS `t_user_order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_order_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL COMMENT '订单id',
  `order_ns` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单号',
  `buyer_id` int(11) unsigned NOT NULL COMMENT '购买人',
  `buyer_nickname` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '购买人昵称',
  `buyer_msg` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户留言（备注）',
  `seller_id` int(11) unsigned DEFAULT NULL COMMENT '卖家id',
  `dist_user_id` int(10) unsigned DEFAULT NULL COMMENT '分销员',
  `dist_amount` decimal(10,2) DEFAULT NULL COMMENT '分销金额',
  `product_id` int(11) unsigned DEFAULT NULL COMMENT '产品id',
  `product_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品的类别，默认是 product ，但是未来可能是 模板、文件、视频等等...',
  `product_type_text` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品标题',
  `product_summary` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sku_id` int(11) DEFAULT NULL COMMENT '商品规格表(t_product_sku)id',
  `product_spec` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品规格(不同规格价格不同)',
  `product_thumbnail` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品缩略图',
  `product_link` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '产品链接',
  `product_price` decimal(10,2) DEFAULT NULL COMMENT '产品价格',
  `product_count` int(11) DEFAULT NULL COMMENT '产品数量',
  `with_virtual` tinyint(1) DEFAULT NULL COMMENT '是否是虚拟产品，1是，0不是。虚拟产品支付完毕后立即交易完成。是虚拟产品，虚拟产品支付完毕后立即交易完成',
  `with_refund` tinyint(1) DEFAULT NULL COMMENT '是否支持退款，1支持，0不支持。',
  `delivery_cost` decimal(10,2) DEFAULT NULL COMMENT '快递费',
  `delivery_id` int(11) unsigned DEFAULT NULL COMMENT '快递单 id',
  `other_cost` decimal(10,2) DEFAULT NULL COMMENT '其它费用',
  `other_cost_remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '其它费用说明',
  `total_amount` decimal(10,2) DEFAULT NULL COMMENT '具体金额 = 产品价格+运费+其他费用- 分销金额',
  `pay_amount` decimal(10,2) DEFAULT NULL COMMENT '支付金额 = 产品价格+运费+其他价格',
  `view_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '查看的网址路径，访问时时，会添加orderid',
  `view_text` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '查看的文章内容，比如：查看、下载',
  `view_effective_time` int(11) unsigned DEFAULT NULL COMMENT '可访问的有效时间，单位秒',
  `comment_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '评论的路径',
  `invoice_id` int(11) unsigned DEFAULT NULL COMMENT '发票',
  `invoice_status` tinyint(2) DEFAULT NULL,
  `item_status` tinyint(2) DEFAULT NULL COMMENT '状态：1交易中、 2交易完成（但是可以申请退款） 、3取消交易 、4申请退款、 5拒绝退款、 6退款中、 7退款完成、 9交易结束',
  `refund_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '退款订单号',
  `refund_amount` decimal(10,2) DEFAULT NULL COMMENT '退款金额',
  `refund_desc` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '退款描述',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='订单明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_order_item`
--

LOCK TABLES `t_user_order_item` WRITE;
/*!40000 ALTER TABLE `t_user_order_item` DISABLE KEYS */;
INSERT INTO `t_user_order_item` VALUES (1,1,'SS001',1,'admin','希望好用',NULL,NULL,NULL,1,'product','产品','iphonexr手机','2020款',NULL,'64G,白色,国行',NULL,NULL,4088.00,2,NULL,NULL,NULL,NULL,NULL,NULL,9166.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-27 21:27:18','2018-08-27 21:27:18'),(2,1,'SS002',1,'ADMIN','听着怎么样',NULL,NULL,NULL,NULL,'product','产品','耳机','苹果耳机',NULL,'128G,黑色,日版',NULL,NULL,199.00,2,NULL,NULL,NULL,NULL,NULL,NULL,388.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-27 21:27:18','2018-08-27 21:27:18'),(6,5,'CE462334999878971392',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关于大到',NULL,NULL,'规格:1.2米*1.5,重量:3KG,刀柄类型:木制',NULL,NULL,23.00,3,NULL,NULL,NULL,NULL,NULL,NULL,69.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,6,'YU462453069989556224',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.2米*1.5,重量:3KG,刀柄类型:木制',NULL,NULL,23.00,1,NULL,NULL,NULL,NULL,NULL,NULL,23.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,7,'HT463698069297573888',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.2米*1.5,重量:4KG,刀柄类型:钢制',NULL,NULL,28.00,1,NULL,NULL,NULL,NULL,NULL,NULL,28.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,8,'ZJ463699327358087168',1,'admin',NULL,NULL,NULL,NULL,7,'product','产品','拳击手套',NULL,NULL,'规格:常规',NULL,NULL,33.00,1,NULL,NULL,NULL,NULL,NULL,NULL,33.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,9,'PI463699406777233408',1,'admin',NULL,NULL,NULL,NULL,7,'product','产品','拳击手套',NULL,NULL,'规格:常规',NULL,NULL,33.00,2,NULL,NULL,NULL,NULL,NULL,NULL,66.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,10,'VT464025601792290816',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,2,NULL,NULL,NULL,NULL,NULL,NULL,48.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,11,'DD464108623275241472',1,'admin',NULL,NULL,NULL,NULL,4,'product','产品','登录鞋',NULL,NULL,'规格:41',NULL,NULL,26.00,1,NULL,NULL,NULL,NULL,NULL,NULL,26.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,11,'DD464108623275241472',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,1,NULL,NULL,NULL,NULL,NULL,NULL,29.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,12,'TF464108958345605120',1,'admin',NULL,NULL,NULL,NULL,3,'product','产品','武术服',NULL,NULL,'颜色:白,尺码:M',NULL,NULL,36.00,1,NULL,NULL,NULL,NULL,NULL,NULL,36.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,13,'DG464110628752338944',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,1,NULL,NULL,NULL,NULL,NULL,NULL,24.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,14,'BT464112339822850048',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,1,NULL,NULL,NULL,NULL,NULL,NULL,24.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,15,'LG464115680023949312',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,1,NULL,NULL,NULL,NULL,NULL,NULL,24.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,16,'LF464116951518818304',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,1,NULL,NULL,NULL,NULL,NULL,NULL,24.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,17,'NZ464120004909215744',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,1,NULL,NULL,NULL,NULL,NULL,NULL,29.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,18,'PC464120043240960000',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,1,NULL,NULL,NULL,NULL,NULL,NULL,29.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,19,'JL464120301559754752',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,1,NULL,NULL,NULL,NULL,NULL,NULL,29.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,20,'VV464121554423525376',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,2,NULL,NULL,NULL,NULL,NULL,NULL,58.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,21,'MM464247848264019968',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,1,NULL,NULL,NULL,NULL,NULL,NULL,24.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,21,'MM464247848264019968',1,'admin',NULL,NULL,NULL,NULL,3,'product','产品','武术服',NULL,NULL,'颜色:白,尺码:M',NULL,NULL,36.00,1,NULL,NULL,NULL,NULL,NULL,NULL,36.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,21,'MM464247848264019968',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,1,NULL,NULL,NULL,NULL,NULL,NULL,29.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,22,'DP464251118764175360',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:5KG,刀柄类型:木制',NULL,NULL,29.00,1,NULL,NULL,NULL,NULL,NULL,NULL,29.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,22,'DP464251118764175360',1,'admin',NULL,NULL,NULL,NULL,3,'product','产品','武术服',NULL,NULL,'颜色:白,尺码:M',NULL,NULL,36.00,1,NULL,NULL,NULL,NULL,NULL,NULL,36.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,23,'SJ464255645672288256',1,'admin',NULL,NULL,NULL,NULL,7,'product','产品','拳击手套',NULL,NULL,'规格:常规',NULL,NULL,33.00,1,NULL,NULL,NULL,NULL,NULL,NULL,33.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,24,'OC464594334789152768',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,2,NULL,NULL,NULL,NULL,NULL,NULL,48.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,25,'NA464605843908997120',1,'admin',NULL,NULL,NULL,NULL,3,'product','产品','武术服',NULL,NULL,'颜色:白,尺码:M',NULL,NULL,36.00,1,NULL,NULL,NULL,NULL,NULL,NULL,36.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,26,'AD465536012626702336',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,1,NULL,NULL,NULL,NULL,NULL,NULL,24.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,28,'KY465663656496214016',1,'admin',NULL,NULL,NULL,NULL,8,'product','产品','记录仪',NULL,NULL,'规格:常规',NULL,NULL,89.00,1,NULL,NULL,NULL,NULL,NULL,NULL,89.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,27,'XI465663656496214016',1,'admin',NULL,NULL,NULL,NULL,8,'product','产品','记录仪',NULL,NULL,'规格:常规',NULL,NULL,89.00,1,NULL,NULL,NULL,NULL,NULL,NULL,89.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,29,'PE466684007569174528',1,'admin',NULL,NULL,NULL,NULL,1,'product','产品','关工大刀',NULL,NULL,'规格:1.8米*1.8,重量:4KG,刀柄类型:钢制',NULL,NULL,24.00,2,NULL,NULL,NULL,NULL,NULL,NULL,48.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,30,'YZ468590094136389632',1,'admin',NULL,NULL,NULL,NULL,14,'product','产品','三少爷的剑',NULL,NULL,'规格:标准',NULL,NULL,33.00,1,NULL,NULL,NULL,NULL,NULL,NULL,33.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,31,'KT468608572834721792',1,'admin',NULL,NULL,NULL,NULL,3,'product','产品','武术服',NULL,NULL,'颜色:白,尺码:M',NULL,NULL,36.00,1,NULL,NULL,NULL,NULL,NULL,NULL,36.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `t_user_order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_role`
--

DROP TABLE IF EXISTS `t_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user_role` (
  `USER_ID` bigint(20) NOT NULL COMMENT '用户ID',
  `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_role`
--

LOCK TABLES `t_user_role` WRITE;
/*!40000 ALTER TABLE `t_user_role` DISABLE KEYS */;
INSERT INTO `t_user_role` VALUES (1,1),(173,2),(2,2),(2,4);
/*!40000 ALTER TABLE `t_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_userconnection`
--

DROP TABLE IF EXISTS `t_userconnection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_userconnection` (
  `userId` varchar(255) NOT NULL,
  `providerId` varchar(255) NOT NULL,
  `providerUserId` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `profileUrl` varchar(512) DEFAULT NULL,
  `imageUrl` varchar(512) DEFAULT NULL,
  `accessToken` varchar(512) NOT NULL,
  `secret` varchar(512) DEFAULT NULL,
  `refreshToken` varchar(512) DEFAULT NULL,
  `expireTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userId`,`providerId`,`providerUserId`),
  UNIQUE KEY `UserConnectionRank` (`userId`,`providerId`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_userconnection`
--

LOCK TABLES `t_userconnection` WRITE;
/*!40000 ALTER TABLE `t_userconnection` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_userconnection` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-19  9:17:53
