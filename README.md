# febs内容管理

#### 概要介绍
业余时间学习springBoot及周边生态框架知识的一个的实践练习工程，不断在学习中，因为一直对CMS和电商类产品比较感觉兴趣但一直没有做过，功能方面参照了jpress和litemall两个优秀应用的设计思想和操作流程,如果对您学习有帮助请点赞

基于开源框架springBoot2.3+thymeleaf+springSecurity+lucene8.5.2 
是在FEBS-Security
权限管理脚手架上增加了cms内容管理和商城管理以及会员中心三个主要模块并集成lucene对商品全文检索,升级了springboot到了最新2.3版本,同时采用 Light Year Admin模板做为管理后台前端.
目前还有很多功能不完善，尤其是模板前端部分。  
目前前端输出都使用的是thymeleaf自定义标签，标签都在febs-web工程com.febs.web.common.thymeleaf.cms.directive包下面，基本已经都能满足目前前端内容输出要求。
很多业务流程可能和真实应用场景有差距，很多功能都实现都是自己想，目前主要实现功能为主，文档后期逐渐补充，需要不断改进优化
由于工作比较忙做的比较仓促，需要完善数据校验，功能后期也会逐渐完善。


#### 功能模块
```
├─系统管理
│  ├─字典管理
│  ├─用户管理
│  ├─菜单管理
│  ├─角色管理
│  └─部门管理
├─系统监控
│  ├─在线用户
│  └─系统日志
├─任务调度
│  ├─定时任务
│  └─调度日志
└─内容管理
│  ├─分类管理 
│  ├─文章管理
│  ├─单页管理
│  ├─tag管理
|  ├─模板主题管理
│  └─评论管理 
└─商城管理
|  ├─分类管理 
|  ├─商品管理
|  ├─订单管理
|  ├─专题管理
|  └─优惠券管理 
└─会员中心
|  ├─文章投递   
|  ├─我的收藏  
|  ├─我的评论  
|  ├─我的订单  
|  └─地址管理   
        
```
## 技术选型

### 后端

- 基础框架：Spring Boot 2.3.0.RELEASE
- 持久层框架：Mybatis 3.5.5
- 安全框架：Spring Security 5.3.2
- 摸板引擎：Thymeleaf 3.0.11
- 全文检索：lucene8.5.2
- 数据库连接池：Hikari
- 缓存框架：Redis
- 日志打印：logback
- 其他：fastjson，poi，javacsv，quartz等。

### 前端
- 基础框架： Light Year Admin(基于bootstrap3.3)  
- JavaScript框架：jQuery
- 消息组件：Bootstrap notify
- 提示框插件：SweetAlert2
- 树形插件：jsTree
- 树形表格插件：jqTreeGrid
- 表格插件：BootstrapTable
- 表单校验插件：jQuery-validate
- 图表插件：Highcharts
- 时间插件：daterangepicker

### 开发环境
- 语言：Java 8
- IDE：Spring Tool Suite 4 
- 依赖管理：Maven
- 数据库：MySQL8.0
- 版本管理：git
## 模块说明
系统分为以下五个模块：
<table>
<tr>
	<th>模块</th>
	<th>说明</th>
</tr>
<tr>
	<td>febs-common</td>
	<td>基础模块，主要包含一些工具类，基础配置</td>
</tr>	
<tr>
	<td>febs-service</td>
	<td>数据库服务和全文检索服务模块，增删改查服务</td>
</tr>
<tr>
	<td>febs-quartz</td>
	<td>任务调度模块，处理定时任务</td>
</tr>
<tr>
	<td>febs-security</td>
	<td>安全模块，和安全有关的都在这个模块里</td>
</tr>
<tr>
	<td>febs-web</td>
	<td>web模块，包含前端部分和控制层</td>
</tr>
</table>

## 应用截图  

![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/220857_492bc90f_496.png "截屏2020-06-03 21.30.03.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/221016_86aac772_496.png "截屏2020-06-03 21.30.20.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/221033_abb21b80_496.png "截屏2020-06-03 21.30.31.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/221058_291fe7cb_496.png "截屏2020-06-03 21.35.01.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/164018_ee604390_496.png "localhost_8081_index.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/200533_61b81408_496.png "截屏2020-06-13 19.52.21.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/221120_4e43e8e9_496.png "截屏2020-06-03 21.41.49.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/164323_a249b1de_496.png "localhost_8081_product_view_wushubiaoyandao.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/221309_ef20e001_496.png "截屏2020-06-03 22.12.43.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/221433_6044671e_496.png "截屏2020-06-03 22.14.00.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/223033_d7ad2dc4_496.png "截屏2020-06-03 22.29.43.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/223045_66140792_496.png "截屏2020-06-03 22.30.11.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/223524_d78fbbe8_496.png "截屏2020-06-03 22.34.49.png")

测试帐号:admin  123456

前台访问地址: http://localhost:8081/

个人中心登录: http://localhost:8081/member/login

后台登录: http://localhost:8081/admin/login

## 启动方式  

1.IDE导入febs工程并进行maven安装依赖  

2.启动redis  

3.创建数据库febs(Character set:utf8,collation:utf8-bin)并导入脚本febs.sql  

4.修改application.yml数据库连接配置和redis连接配置  

5.运行febs-web工程下FebsApplication.java方法启动工程  

6.启动完成登录后台管理，商城管理->商品->重新索引 进行商品信息索引，前台才可以检索到

## 开发计划

1. 完善前端功能和界面优化(进行中),后端代码规范梳理，完善数据校验和错误处理机制

2. 增加jwt接口权限校验，便于app接口调用(进行中) 

3. 全文检索功能完善（学习研究中） 

4. 对接支付接口

5. 完善订单处理逻辑

## 致谢

本项目基于或参考以下项目：

1. [febs-security](https://github.com/febsteam/FEBS-Security)  

   项目参考：以此项目为权限脚手架进行扩展

2. [jpress](https://gitee.com/fuhai/jpress)  
  
   项目参考：操作界面和功能逻辑

3. [ Light Year Admin](https://gitee.com/yinqi/Light-Year-Admin-Template)  

   项目参考：以此做为后台管理系统模板

3. [ litemall](https://gitee.com/linlinjava/litemall)  

   项目参考：部分商品和订单操作流程参考