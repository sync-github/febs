package com.febs.search.lucene.model;/**
 * Created by Administrator on 2019/11/12/012.
 */


/**
 * @ClassName Sort
 * @Description TODO
 * @Author yb
 * @Date 2019/11/12/01219:29
 * @Version 1.0
 **/
public class Sort {
    private String order;
    private String Field;
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getField() {
		return Field;
	}
	public void setField(String field) {
		Field = field;
	}
    
}
